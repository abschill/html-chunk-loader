export declare const useInlineScope: (input: string) => string;
export declare const useLoopOpenScope: (name: string) => string;
