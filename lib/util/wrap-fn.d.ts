export declare function wrap<T>(t: () => T, c: () => T): T;
