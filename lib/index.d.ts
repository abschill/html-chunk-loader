import { HTMLChunkLoader, USSROptions } from './types';
export declare function useLoader(config?: USSROptions): HTMLChunkLoader;
export declare function createLoader(u_config?: USSROptions): HTMLChunkLoader;
