[html-chunk-loader - v0.7.0](../README.md) / [Modules](../modules.md) / [Types](../modules/Types.md) / DebugEventStatus

# Enumeration: DebugEventStatus

[Types](../modules/Types.md).DebugEventStatus

## Table of contents

### Enumeration Members

- [CRITICAL](Types.DebugEventStatus.md#critical)
- [DEFAULT](Types.DebugEventStatus.md#default)
- [VERBOSE](Types.DebugEventStatus.md#verbose)

## Enumeration Members

### CRITICAL

• **CRITICAL**

#### Defined in

[types/index.ts:99](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L99)

___

### DEFAULT

• **DEFAULT**

#### Defined in

[types/index.ts:98](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L98)

___

### VERBOSE

• **VERBOSE**

#### Defined in

[types/index.ts:97](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L97)
