[html-chunk-loader - v0.7.0](../README.md) / [Modules](../modules.md) / [Types](../modules/Types.md) / DebugEventPhase

# Enumeration: DebugEventPhase

[Types](../modules/Types.md).DebugEventPhase

## Table of contents

### Enumeration Members

- [CHUNK\_RENDER](Types.DebugEventPhase.md#chunk_render)
- [CHUNK\_RESOLVE](Types.DebugEventPhase.md#chunk_resolve)
- [CHUNK\_TOKENIZE](Types.DebugEventPhase.md#chunk_tokenize)
- [RUNTIME\_INIT](Types.DebugEventPhase.md#runtime_init)
- [UNSPECIFIED](Types.DebugEventPhase.md#unspecified)

## Enumeration Members

### CHUNK\_RENDER

• **CHUNK\_RENDER**

#### Defined in

[types/index.ts:93](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L93)

___

### CHUNK\_RESOLVE

• **CHUNK\_RESOLVE**

#### Defined in

[types/index.ts:91](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L91)

___

### CHUNK\_TOKENIZE

• **CHUNK\_TOKENIZE**

#### Defined in

[types/index.ts:92](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L92)

___

### RUNTIME\_INIT

• **RUNTIME\_INIT**

#### Defined in

[types/index.ts:90](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L90)

___

### UNSPECIFIED

• **UNSPECIFIED**

#### Defined in

[types/index.ts:89](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L89)
