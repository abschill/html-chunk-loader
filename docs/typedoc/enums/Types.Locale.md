[html-chunk-loader - v0.7.0](../README.md) / [Modules](../modules.md) / [Types](../modules/Types.md) / Locale

# Enumeration: Locale

[Types](../modules/Types.md).Locale

Locale Enums

## Table of contents

### Enumeration Members

- [af](Types.Locale.md#af)
- [af\_NA](Types.Locale.md#af_na)
- [af\_ZA](Types.Locale.md#af_za)
- [agq](Types.Locale.md#agq)
- [agq\_CM](Types.Locale.md#agq_cm)
- [ak](Types.Locale.md#ak)
- [ak\_GH](Types.Locale.md#ak_gh)
- [am](Types.Locale.md#am)
- [am\_ET](Types.Locale.md#am_et)
- [ar](Types.Locale.md#ar)
- [ar\_001](Types.Locale.md#ar_001)
- [ar\_AE](Types.Locale.md#ar_ae)
- [ar\_BH](Types.Locale.md#ar_bh)
- [ar\_DZ](Types.Locale.md#ar_dz)
- [ar\_EG](Types.Locale.md#ar_eg)
- [ar\_IQ](Types.Locale.md#ar_iq)
- [ar\_JO](Types.Locale.md#ar_jo)
- [ar\_KW](Types.Locale.md#ar_kw)
- [ar\_LB](Types.Locale.md#ar_lb)
- [ar\_LY](Types.Locale.md#ar_ly)
- [ar\_MA](Types.Locale.md#ar_ma)
- [ar\_OM](Types.Locale.md#ar_om)
- [ar\_QA](Types.Locale.md#ar_qa)
- [ar\_SA](Types.Locale.md#ar_sa)
- [ar\_SD](Types.Locale.md#ar_sd)
- [ar\_SY](Types.Locale.md#ar_sy)
- [ar\_TN](Types.Locale.md#ar_tn)
- [ar\_YE](Types.Locale.md#ar_ye)
- [as](Types.Locale.md#as)
- [as\_IN](Types.Locale.md#as_in)
- [asa](Types.Locale.md#asa)
- [asa\_TZ](Types.Locale.md#asa_tz)
- [az](Types.Locale.md#az)
- [az\_Cyrl](Types.Locale.md#az_cyrl)
- [az\_Cyrl\_AZ](Types.Locale.md#az_cyrl_az)
- [az\_Latn](Types.Locale.md#az_latn)
- [az\_Latn\_AZ](Types.Locale.md#az_latn_az)
- [bas](Types.Locale.md#bas)
- [bas\_CM](Types.Locale.md#bas_cm)
- [be](Types.Locale.md#be)
- [be\_BY](Types.Locale.md#be_by)
- [bem](Types.Locale.md#bem)
- [bem\_ZM](Types.Locale.md#bem_zm)
- [bez](Types.Locale.md#bez)
- [bez\_TZ](Types.Locale.md#bez_tz)
- [bg](Types.Locale.md#bg)
- [bg\_BG](Types.Locale.md#bg_bg)
- [bm](Types.Locale.md#bm)
- [bm\_ML](Types.Locale.md#bm_ml)
- [bn](Types.Locale.md#bn)
- [bn\_BD](Types.Locale.md#bn_bd)
- [bn\_IN](Types.Locale.md#bn_in)
- [bo](Types.Locale.md#bo)
- [bo\_CN](Types.Locale.md#bo_cn)
- [bo\_IN](Types.Locale.md#bo_in)
- [br](Types.Locale.md#br)
- [br\_FR](Types.Locale.md#br_fr)
- [brx](Types.Locale.md#brx)
- [brx\_IN](Types.Locale.md#brx_in)
- [bs](Types.Locale.md#bs)
- [bs\_BA](Types.Locale.md#bs_ba)
- [ca](Types.Locale.md#ca)
- [ca\_ES](Types.Locale.md#ca_es)
- [cgg](Types.Locale.md#cgg)
- [cgg\_UG](Types.Locale.md#cgg_ug)
- [chr](Types.Locale.md#chr)
- [chr\_US](Types.Locale.md#chr_us)
- [cs](Types.Locale.md#cs)
- [cs\_CZ](Types.Locale.md#cs_cz)
- [cy](Types.Locale.md#cy)
- [cy\_GB](Types.Locale.md#cy_gb)
- [da](Types.Locale.md#da)
- [da\_DK](Types.Locale.md#da_dk)
- [dav](Types.Locale.md#dav)
- [dav\_KE](Types.Locale.md#dav_ke)
- [de](Types.Locale.md#de)
- [de\_AT](Types.Locale.md#de_at)
- [de\_BE](Types.Locale.md#de_be)
- [de\_CH](Types.Locale.md#de_ch)
- [de\_DE](Types.Locale.md#de_de)
- [de\_LI](Types.Locale.md#de_li)
- [de\_LU](Types.Locale.md#de_lu)
- [dje](Types.Locale.md#dje)
- [dje\_NE](Types.Locale.md#dje_ne)
- [dua](Types.Locale.md#dua)
- [dua\_CM](Types.Locale.md#dua_cm)
- [dyo](Types.Locale.md#dyo)
- [dyo\_SN](Types.Locale.md#dyo_sn)
- [ebu](Types.Locale.md#ebu)
- [ebu\_KE](Types.Locale.md#ebu_ke)
- [ee](Types.Locale.md#ee)
- [ee\_GH](Types.Locale.md#ee_gh)
- [ee\_TG](Types.Locale.md#ee_tg)
- [el](Types.Locale.md#el)
- [el\_CY](Types.Locale.md#el_cy)
- [el\_GR](Types.Locale.md#el_gr)
- [en](Types.Locale.md#en)
- [en\_AE](Types.Locale.md#en_ae)
- [en\_AR](Types.Locale.md#en_ar)
- [en\_AS](Types.Locale.md#en_as)
- [en\_AT](Types.Locale.md#en_at)
- [en\_AU](Types.Locale.md#en_au)
- [en\_BB](Types.Locale.md#en_bb)
- [en\_BE](Types.Locale.md#en_be)
- [en\_BG](Types.Locale.md#en_bg)
- [en\_BH](Types.Locale.md#en_bh)
- [en\_BM](Types.Locale.md#en_bm)
- [en\_BR](Types.Locale.md#en_br)
- [en\_BW](Types.Locale.md#en_bw)
- [en\_BZ](Types.Locale.md#en_bz)
- [en\_CA](Types.Locale.md#en_ca)
- [en\_CH](Types.Locale.md#en_ch)
- [en\_CL](Types.Locale.md#en_cl)
- [en\_CN](Types.Locale.md#en_cn)
- [en\_CO](Types.Locale.md#en_co)
- [en\_CR](Types.Locale.md#en_cr)
- [en\_CY](Types.Locale.md#en_cy)
- [en\_CZ](Types.Locale.md#en_cz)
- [en\_DE](Types.Locale.md#en_de)
- [en\_DK](Types.Locale.md#en_dk)
- [en\_DO](Types.Locale.md#en_do)
- [en\_EE](Types.Locale.md#en_ee)
- [en\_EG](Types.Locale.md#en_eg)
- [en\_ES](Types.Locale.md#en_es)
- [en\_FI](Types.Locale.md#en_fi)
- [en\_GB](Types.Locale.md#en_gb)
- [en\_GE](Types.Locale.md#en_ge)
- [en\_GF](Types.Locale.md#en_gf)
- [en\_GH](Types.Locale.md#en_gh)
- [en\_GI](Types.Locale.md#en_gi)
- [en\_GR](Types.Locale.md#en_gr)
- [en\_GU](Types.Locale.md#en_gu)
- [en\_GY](Types.Locale.md#en_gy)
- [en\_HK](Types.Locale.md#en_hk)
- [en\_HR](Types.Locale.md#en_hr)
- [en\_HU](Types.Locale.md#en_hu)
- [en\_ID](Types.Locale.md#en_id)
- [en\_IE](Types.Locale.md#en_ie)
- [en\_IL](Types.Locale.md#en_il)
- [en\_IN](Types.Locale.md#en_in)
- [en\_IS](Types.Locale.md#en_is)
- [en\_IT](Types.Locale.md#en_it)
- [en\_JM](Types.Locale.md#en_jm)
- [en\_JO](Types.Locale.md#en_jo)
- [en\_JP](Types.Locale.md#en_jp)
- [en\_KR](Types.Locale.md#en_kr)
- [en\_KW](Types.Locale.md#en_kw)
- [en\_KY](Types.Locale.md#en_ky)
- [en\_LB](Types.Locale.md#en_lb)
- [en\_LI](Types.Locale.md#en_li)
- [en\_LT](Types.Locale.md#en_lt)
- [en\_LU](Types.Locale.md#en_lu)
- [en\_LV](Types.Locale.md#en_lv)
- [en\_MA](Types.Locale.md#en_ma)
- [en\_MC](Types.Locale.md#en_mc)
- [en\_MG](Types.Locale.md#en_mg)
- [en\_MH](Types.Locale.md#en_mh)
- [en\_MK](Types.Locale.md#en_mk)
- [en\_MO](Types.Locale.md#en_mo)
- [en\_MP](Types.Locale.md#en_mp)
- [en\_MT](Types.Locale.md#en_mt)
- [en\_MU](Types.Locale.md#en_mu)
- [en\_MX](Types.Locale.md#en_mx)
- [en\_MY](Types.Locale.md#en_my)
- [en\_NA](Types.Locale.md#en_na)
- [en\_NL](Types.Locale.md#en_nl)
- [en\_NO](Types.Locale.md#en_no)
- [en\_NZ](Types.Locale.md#en_nz)
- [en\_OM](Types.Locale.md#en_om)
- [en\_PE](Types.Locale.md#en_pe)
- [en\_PH](Types.Locale.md#en_ph)
- [en\_PK](Types.Locale.md#en_pk)
- [en\_PL](Types.Locale.md#en_pl)
- [en\_PR](Types.Locale.md#en_pr)
- [en\_PT](Types.Locale.md#en_pt)
- [en\_PY](Types.Locale.md#en_py)
- [en\_QA](Types.Locale.md#en_qa)
- [en\_RO](Types.Locale.md#en_ro)
- [en\_RU](Types.Locale.md#en_ru)
- [en\_SA](Types.Locale.md#en_sa)
- [en\_SE](Types.Locale.md#en_se)
- [en\_SG](Types.Locale.md#en_sg)
- [en\_SI](Types.Locale.md#en_si)
- [en\_SK](Types.Locale.md#en_sk)
- [en\_TH](Types.Locale.md#en_th)
- [en\_TR](Types.Locale.md#en_tr)
- [en\_TT](Types.Locale.md#en_tt)
- [en\_TW](Types.Locale.md#en_tw)
- [en\_UA](Types.Locale.md#en_ua)
- [en\_UM](Types.Locale.md#en_um)
- [en\_US](Types.Locale.md#en_us)
- [en\_US\_POSIX](Types.Locale.md#en_us_posix)
- [en\_UY](Types.Locale.md#en_uy)
- [en\_VE](Types.Locale.md#en_ve)
- [en\_VI](Types.Locale.md#en_vi)
- [en\_VN](Types.Locale.md#en_vn)
- [en\_ZA](Types.Locale.md#en_za)
- [en\_ZW](Types.Locale.md#en_zw)
- [eo](Types.Locale.md#eo)
- [es](Types.Locale.md#es)
- [es\_419](Types.Locale.md#es_419)
- [es\_AR](Types.Locale.md#es_ar)
- [es\_BO](Types.Locale.md#es_bo)
- [es\_CL](Types.Locale.md#es_cl)
- [es\_CO](Types.Locale.md#es_co)
- [es\_CR](Types.Locale.md#es_cr)
- [es\_DO](Types.Locale.md#es_do)
- [es\_EC](Types.Locale.md#es_ec)
- [es\_ES](Types.Locale.md#es_es)
- [es\_GQ](Types.Locale.md#es_gq)
- [es\_GT](Types.Locale.md#es_gt)
- [es\_HN](Types.Locale.md#es_hn)
- [es\_MX](Types.Locale.md#es_mx)
- [es\_NI](Types.Locale.md#es_ni)
- [es\_PA](Types.Locale.md#es_pa)
- [es\_PE](Types.Locale.md#es_pe)
- [es\_PR](Types.Locale.md#es_pr)
- [es\_PY](Types.Locale.md#es_py)
- [es\_SV](Types.Locale.md#es_sv)
- [es\_US](Types.Locale.md#es_us)
- [es\_UY](Types.Locale.md#es_uy)
- [es\_VE](Types.Locale.md#es_ve)
- [et](Types.Locale.md#et)
- [et\_EE](Types.Locale.md#et_ee)
- [eu](Types.Locale.md#eu)
- [eu\_ES](Types.Locale.md#eu_es)
- [ewo](Types.Locale.md#ewo)
- [ewo\_CM](Types.Locale.md#ewo_cm)
- [fa](Types.Locale.md#fa)
- [fa\_AF](Types.Locale.md#fa_af)
- [fa\_IR](Types.Locale.md#fa_ir)
- [ff](Types.Locale.md#ff)
- [ff\_SN](Types.Locale.md#ff_sn)
- [fi](Types.Locale.md#fi)
- [fi\_FI](Types.Locale.md#fi_fi)
- [fil](Types.Locale.md#fil)
- [fil\_PH](Types.Locale.md#fil_ph)
- [fo](Types.Locale.md#fo)
- [fo\_FO](Types.Locale.md#fo_fo)
- [fr](Types.Locale.md#fr)
- [fr\_BE](Types.Locale.md#fr_be)
- [fr\_BF](Types.Locale.md#fr_bf)
- [fr\_BI](Types.Locale.md#fr_bi)
- [fr\_BJ](Types.Locale.md#fr_bj)
- [fr\_BL](Types.Locale.md#fr_bl)
- [fr\_CA](Types.Locale.md#fr_ca)
- [fr\_CD](Types.Locale.md#fr_cd)
- [fr\_CF](Types.Locale.md#fr_cf)
- [fr\_CG](Types.Locale.md#fr_cg)
- [fr\_CH](Types.Locale.md#fr_ch)
- [fr\_CI](Types.Locale.md#fr_ci)
- [fr\_CM](Types.Locale.md#fr_cm)
- [fr\_DJ](Types.Locale.md#fr_dj)
- [fr\_FR](Types.Locale.md#fr_fr)
- [fr\_GA](Types.Locale.md#fr_ga)
- [fr\_GF](Types.Locale.md#fr_gf)
- [fr\_GN](Types.Locale.md#fr_gn)
- [fr\_GP](Types.Locale.md#fr_gp)
- [fr\_GQ](Types.Locale.md#fr_gq)
- [fr\_KM](Types.Locale.md#fr_km)
- [fr\_LU](Types.Locale.md#fr_lu)
- [fr\_MC](Types.Locale.md#fr_mc)
- [fr\_MF](Types.Locale.md#fr_mf)
- [fr\_MG](Types.Locale.md#fr_mg)
- [fr\_ML](Types.Locale.md#fr_ml)
- [fr\_MQ](Types.Locale.md#fr_mq)
- [fr\_NE](Types.Locale.md#fr_ne)
- [fr\_RE](Types.Locale.md#fr_re)
- [fr\_RW](Types.Locale.md#fr_rw)
- [fr\_SN](Types.Locale.md#fr_sn)
- [fr\_TD](Types.Locale.md#fr_td)
- [fr\_TG](Types.Locale.md#fr_tg)
- [fr\_YT](Types.Locale.md#fr_yt)
- [ga](Types.Locale.md#ga)
- [ga\_IE](Types.Locale.md#ga_ie)
- [gl](Types.Locale.md#gl)
- [gl\_ES](Types.Locale.md#gl_es)
- [gsw](Types.Locale.md#gsw)
- [gsw\_CH](Types.Locale.md#gsw_ch)
- [gu](Types.Locale.md#gu)
- [gu\_IN](Types.Locale.md#gu_in)
- [guz](Types.Locale.md#guz)
- [guz\_KE](Types.Locale.md#guz_ke)
- [gv](Types.Locale.md#gv)
- [gv\_GB](Types.Locale.md#gv_gb)
- [ha](Types.Locale.md#ha)
- [ha\_Latn](Types.Locale.md#ha_latn)
- [ha\_Latn\_GH](Types.Locale.md#ha_latn_gh)
- [ha\_Latn\_NE](Types.Locale.md#ha_latn_ne)
- [ha\_Latn\_NG](Types.Locale.md#ha_latn_ng)
- [haw](Types.Locale.md#haw)
- [haw\_US](Types.Locale.md#haw_us)
- [he](Types.Locale.md#he)
- [he\_IL](Types.Locale.md#he_il)
- [hi](Types.Locale.md#hi)
- [hi\_IN](Types.Locale.md#hi_in)
- [hr](Types.Locale.md#hr)
- [hr\_HR](Types.Locale.md#hr_hr)
- [hu](Types.Locale.md#hu)
- [hu\_HU](Types.Locale.md#hu_hu)
- [hy](Types.Locale.md#hy)
- [hy\_AM](Types.Locale.md#hy_am)
- [id](Types.Locale.md#id)
- [id\_ID](Types.Locale.md#id_id)
- [ig](Types.Locale.md#ig)
- [ig\_NG](Types.Locale.md#ig_ng)
- [ii](Types.Locale.md#ii)
- [ii\_CN](Types.Locale.md#ii_cn)
- [is](Types.Locale.md#is)
- [is\_IS](Types.Locale.md#is_is)
- [it](Types.Locale.md#it)
- [it\_CH](Types.Locale.md#it_ch)
- [it\_IT](Types.Locale.md#it_it)
- [ja](Types.Locale.md#ja)
- [ja\_JP](Types.Locale.md#ja_jp)
- [jmc](Types.Locale.md#jmc)
- [jmc\_TZ](Types.Locale.md#jmc_tz)
- [ka](Types.Locale.md#ka)
- [ka\_GE](Types.Locale.md#ka_ge)
- [kab](Types.Locale.md#kab)
- [kab\_DZ](Types.Locale.md#kab_dz)
- [kam](Types.Locale.md#kam)
- [kam\_KE](Types.Locale.md#kam_ke)
- [kde](Types.Locale.md#kde)
- [kde\_TZ](Types.Locale.md#kde_tz)
- [kea](Types.Locale.md#kea)
- [kea\_CV](Types.Locale.md#kea_cv)
- [khq](Types.Locale.md#khq)
- [khq\_ML](Types.Locale.md#khq_ml)
- [ki](Types.Locale.md#ki)
- [ki\_KE](Types.Locale.md#ki_ke)
- [kk](Types.Locale.md#kk)
- [kk\_Cyrl](Types.Locale.md#kk_cyrl)
- [kk\_Cyrl\_KZ](Types.Locale.md#kk_cyrl_kz)
- [kl](Types.Locale.md#kl)
- [kl\_GL](Types.Locale.md#kl_gl)
- [kln](Types.Locale.md#kln)
- [kln\_KE](Types.Locale.md#kln_ke)
- [km](Types.Locale.md#km)
- [km\_KH](Types.Locale.md#km_kh)
- [kn](Types.Locale.md#kn)
- [kn\_IN](Types.Locale.md#kn_in)
- [ko](Types.Locale.md#ko)
- [ko\_KR](Types.Locale.md#ko_kr)
- [kok](Types.Locale.md#kok)
- [kok\_IN](Types.Locale.md#kok_in)
- [ksb](Types.Locale.md#ksb)
- [ksb\_TZ](Types.Locale.md#ksb_tz)
- [ksf](Types.Locale.md#ksf)
- [ksf\_CM](Types.Locale.md#ksf_cm)
- [kw](Types.Locale.md#kw)
- [kw\_GB](Types.Locale.md#kw_gb)
- [lag](Types.Locale.md#lag)
- [lag\_TZ](Types.Locale.md#lag_tz)
- [lg](Types.Locale.md#lg)
- [lg\_UG](Types.Locale.md#lg_ug)
- [ln](Types.Locale.md#ln)
- [ln\_CD](Types.Locale.md#ln_cd)
- [ln\_CG](Types.Locale.md#ln_cg)
- [lt](Types.Locale.md#lt)
- [lt\_LT](Types.Locale.md#lt_lt)
- [lu](Types.Locale.md#lu)
- [lu\_CD](Types.Locale.md#lu_cd)
- [luo](Types.Locale.md#luo)
- [luo\_KE](Types.Locale.md#luo_ke)
- [luy](Types.Locale.md#luy)
- [luy\_KE](Types.Locale.md#luy_ke)
- [lv](Types.Locale.md#lv)
- [lv\_LV](Types.Locale.md#lv_lv)
- [mas](Types.Locale.md#mas)
- [mas\_KE](Types.Locale.md#mas_ke)
- [mas\_TZ](Types.Locale.md#mas_tz)
- [mer](Types.Locale.md#mer)
- [mer\_KE](Types.Locale.md#mer_ke)
- [mfe](Types.Locale.md#mfe)
- [mfe\_MU](Types.Locale.md#mfe_mu)
- [mg](Types.Locale.md#mg)
- [mg\_MG](Types.Locale.md#mg_mg)
- [mgh](Types.Locale.md#mgh)
- [mgh\_MZ](Types.Locale.md#mgh_mz)
- [mk](Types.Locale.md#mk)
- [mk\_MK](Types.Locale.md#mk_mk)
- [ml](Types.Locale.md#ml)
- [ml\_IN](Types.Locale.md#ml_in)
- [mr](Types.Locale.md#mr)
- [mr\_IN](Types.Locale.md#mr_in)
- [ms](Types.Locale.md#ms)
- [ms\_BN](Types.Locale.md#ms_bn)
- [ms\_MY](Types.Locale.md#ms_my)
- [mt](Types.Locale.md#mt)
- [mt\_MT](Types.Locale.md#mt_mt)
- [mua](Types.Locale.md#mua)
- [mua\_CM](Types.Locale.md#mua_cm)
- [my](Types.Locale.md#my)
- [my\_MM](Types.Locale.md#my_mm)
- [naq](Types.Locale.md#naq)
- [naq\_NA](Types.Locale.md#naq_na)
- [nb](Types.Locale.md#nb)
- [nb\_NO](Types.Locale.md#nb_no)
- [nd](Types.Locale.md#nd)
- [nd\_ZW](Types.Locale.md#nd_zw)
- [ne](Types.Locale.md#ne)
- [ne\_IN](Types.Locale.md#ne_in)
- [ne\_NP](Types.Locale.md#ne_np)
- [nl](Types.Locale.md#nl)
- [nl\_AW](Types.Locale.md#nl_aw)
- [nl\_BE](Types.Locale.md#nl_be)
- [nl\_CW](Types.Locale.md#nl_cw)
- [nl\_NL](Types.Locale.md#nl_nl)
- [nl\_SX](Types.Locale.md#nl_sx)
- [nmg](Types.Locale.md#nmg)
- [nmg\_CM](Types.Locale.md#nmg_cm)
- [nn](Types.Locale.md#nn)
- [nn\_NO](Types.Locale.md#nn_no)
- [nus](Types.Locale.md#nus)
- [nus\_SD](Types.Locale.md#nus_sd)
- [nyn](Types.Locale.md#nyn)
- [nyn\_UG](Types.Locale.md#nyn_ug)
- [om](Types.Locale.md#om)
- [om\_ET](Types.Locale.md#om_et)
- [om\_KE](Types.Locale.md#om_ke)
- [or](Types.Locale.md#or)
- [or\_IN](Types.Locale.md#or_in)
- [pa](Types.Locale.md#pa)
- [pa\_Arab](Types.Locale.md#pa_arab)
- [pa\_Arab\_PK](Types.Locale.md#pa_arab_pk)
- [pa\_Guru](Types.Locale.md#pa_guru)
- [pa\_Guru\_IN](Types.Locale.md#pa_guru_in)
- [pl](Types.Locale.md#pl)
- [pl\_PL](Types.Locale.md#pl_pl)
- [ps](Types.Locale.md#ps)
- [ps\_AF](Types.Locale.md#ps_af)
- [pt](Types.Locale.md#pt)
- [pt\_AO](Types.Locale.md#pt_ao)
- [pt\_BR](Types.Locale.md#pt_br)
- [pt\_GW](Types.Locale.md#pt_gw)
- [pt\_MZ](Types.Locale.md#pt_mz)
- [pt\_PT](Types.Locale.md#pt_pt)
- [pt\_ST](Types.Locale.md#pt_st)
- [rm](Types.Locale.md#rm)
- [rm\_CH](Types.Locale.md#rm_ch)
- [rn](Types.Locale.md#rn)
- [rn\_BI](Types.Locale.md#rn_bi)
- [ro](Types.Locale.md#ro)
- [ro\_MD](Types.Locale.md#ro_md)
- [ro\_RO](Types.Locale.md#ro_ro)
- [rof](Types.Locale.md#rof)
- [rof\_TZ](Types.Locale.md#rof_tz)
- [ru](Types.Locale.md#ru)
- [ru\_MD](Types.Locale.md#ru_md)
- [ru\_RU](Types.Locale.md#ru_ru)
- [ru\_UA](Types.Locale.md#ru_ua)
- [rw](Types.Locale.md#rw)
- [rw\_RW](Types.Locale.md#rw_rw)
- [rwk](Types.Locale.md#rwk)
- [rwk\_TZ](Types.Locale.md#rwk_tz)
- [saq](Types.Locale.md#saq)
- [saq\_KE](Types.Locale.md#saq_ke)
- [sbp](Types.Locale.md#sbp)
- [sbp\_TZ](Types.Locale.md#sbp_tz)
- [seh](Types.Locale.md#seh)
- [seh\_MZ](Types.Locale.md#seh_mz)
- [ses](Types.Locale.md#ses)
- [ses\_ML](Types.Locale.md#ses_ml)
- [sg](Types.Locale.md#sg)
- [sg\_CF](Types.Locale.md#sg_cf)
- [shi](Types.Locale.md#shi)
- [shi\_Latn](Types.Locale.md#shi_latn)
- [shi\_Latn\_MA](Types.Locale.md#shi_latn_ma)
- [shi\_Tfng](Types.Locale.md#shi_tfng)
- [shi\_Tfng\_MA](Types.Locale.md#shi_tfng_ma)
- [si](Types.Locale.md#si)
- [si\_LK](Types.Locale.md#si_lk)
- [sk](Types.Locale.md#sk)
- [sk\_SK](Types.Locale.md#sk_sk)
- [sl](Types.Locale.md#sl)
- [sl\_SI](Types.Locale.md#sl_si)
- [sn](Types.Locale.md#sn)
- [sn\_ZW](Types.Locale.md#sn_zw)
- [so](Types.Locale.md#so)
- [so\_DJ](Types.Locale.md#so_dj)
- [so\_ET](Types.Locale.md#so_et)
- [so\_KE](Types.Locale.md#so_ke)
- [so\_SO](Types.Locale.md#so_so)
- [sq](Types.Locale.md#sq)
- [sq\_AL](Types.Locale.md#sq_al)
- [sr](Types.Locale.md#sr)
- [sr\_Cyrl](Types.Locale.md#sr_cyrl)
- [sr\_Cyrl\_BA](Types.Locale.md#sr_cyrl_ba)
- [sr\_Cyrl\_ME](Types.Locale.md#sr_cyrl_me)
- [sr\_Cyrl\_RS](Types.Locale.md#sr_cyrl_rs)
- [sr\_Latn](Types.Locale.md#sr_latn)
- [sr\_Latn\_BA](Types.Locale.md#sr_latn_ba)
- [sr\_Latn\_ME](Types.Locale.md#sr_latn_me)
- [sr\_Latn\_RS](Types.Locale.md#sr_latn_rs)
- [sv](Types.Locale.md#sv)
- [sv\_FI](Types.Locale.md#sv_fi)
- [sv\_SE](Types.Locale.md#sv_se)
- [sw](Types.Locale.md#sw)
- [sw\_KE](Types.Locale.md#sw_ke)
- [sw\_TZ](Types.Locale.md#sw_tz)
- [swc](Types.Locale.md#swc)
- [swc\_CD](Types.Locale.md#swc_cd)
- [ta](Types.Locale.md#ta)
- [ta\_IN](Types.Locale.md#ta_in)
- [ta\_LK](Types.Locale.md#ta_lk)
- [te](Types.Locale.md#te)
- [te\_IN](Types.Locale.md#te_in)
- [teo](Types.Locale.md#teo)
- [teo\_KE](Types.Locale.md#teo_ke)
- [teo\_UG](Types.Locale.md#teo_ug)
- [th](Types.Locale.md#th)
- [th\_TH](Types.Locale.md#th_th)
- [ti](Types.Locale.md#ti)
- [ti\_ER](Types.Locale.md#ti_er)
- [ti\_ET](Types.Locale.md#ti_et)
- [to](Types.Locale.md#to)
- [to\_TO](Types.Locale.md#to_to)
- [tr](Types.Locale.md#tr)
- [tr\_TR](Types.Locale.md#tr_tr)
- [twq](Types.Locale.md#twq)
- [twq\_NE](Types.Locale.md#twq_ne)
- [tzm](Types.Locale.md#tzm)
- [tzm\_Latn](Types.Locale.md#tzm_latn)
- [tzm\_Latn\_MA](Types.Locale.md#tzm_latn_ma)
- [uk](Types.Locale.md#uk)
- [uk\_UA](Types.Locale.md#uk_ua)
- [ur](Types.Locale.md#ur)
- [ur\_IN](Types.Locale.md#ur_in)
- [ur\_PK](Types.Locale.md#ur_pk)
- [uz](Types.Locale.md#uz)
- [uz\_Arab](Types.Locale.md#uz_arab)
- [uz\_Arab\_AF](Types.Locale.md#uz_arab_af)
- [uz\_Cyrl](Types.Locale.md#uz_cyrl)
- [uz\_Cyrl\_UZ](Types.Locale.md#uz_cyrl_uz)
- [uz\_Latn](Types.Locale.md#uz_latn)
- [uz\_Latn\_UZ](Types.Locale.md#uz_latn_uz)
- [vai](Types.Locale.md#vai)
- [vai\_Latn](Types.Locale.md#vai_latn)
- [vai\_Latn\_LR](Types.Locale.md#vai_latn_lr)
- [vai\_Vaii](Types.Locale.md#vai_vaii)
- [vai\_Vaii\_LR](Types.Locale.md#vai_vaii_lr)
- [vi](Types.Locale.md#vi)
- [vi\_VN](Types.Locale.md#vi_vn)
- [vun](Types.Locale.md#vun)
- [vun\_TZ](Types.Locale.md#vun_tz)
- [xog](Types.Locale.md#xog)
- [xog\_UG](Types.Locale.md#xog_ug)
- [yav](Types.Locale.md#yav)
- [yav\_CM](Types.Locale.md#yav_cm)
- [yo](Types.Locale.md#yo)
- [yo\_NG](Types.Locale.md#yo_ng)
- [zh](Types.Locale.md#zh)
- [zh\_Hans](Types.Locale.md#zh_hans)
- [zh\_Hans\_AE](Types.Locale.md#zh_hans_ae)
- [zh\_Hans\_AR](Types.Locale.md#zh_hans_ar)
- [zh\_Hans\_AT](Types.Locale.md#zh_hans_at)
- [zh\_Hans\_AU](Types.Locale.md#zh_hans_au)
- [zh\_Hans\_BE](Types.Locale.md#zh_hans_be)
- [zh\_Hans\_BG](Types.Locale.md#zh_hans_bg)
- [zh\_Hans\_BH](Types.Locale.md#zh_hans_bh)
- [zh\_Hans\_BR](Types.Locale.md#zh_hans_br)
- [zh\_Hans\_BW](Types.Locale.md#zh_hans_bw)
- [zh\_Hans\_CA](Types.Locale.md#zh_hans_ca)
- [zh\_Hans\_CH](Types.Locale.md#zh_hans_ch)
- [zh\_Hans\_CL](Types.Locale.md#zh_hans_cl)
- [zh\_Hans\_CN](Types.Locale.md#zh_hans_cn)
- [zh\_Hans\_CO](Types.Locale.md#zh_hans_co)
- [zh\_Hans\_CR](Types.Locale.md#zh_hans_cr)
- [zh\_Hans\_CY](Types.Locale.md#zh_hans_cy)
- [zh\_Hans\_CZ](Types.Locale.md#zh_hans_cz)
- [zh\_Hans\_DE](Types.Locale.md#zh_hans_de)
- [zh\_Hans\_DK](Types.Locale.md#zh_hans_dk)
- [zh\_Hans\_DO](Types.Locale.md#zh_hans_do)
- [zh\_Hans\_EE](Types.Locale.md#zh_hans_ee)
- [zh\_Hans\_EG](Types.Locale.md#zh_hans_eg)
- [zh\_Hans\_ES](Types.Locale.md#zh_hans_es)
- [zh\_Hans\_FI](Types.Locale.md#zh_hans_fi)
- [zh\_Hans\_GB](Types.Locale.md#zh_hans_gb)
- [zh\_Hans\_GE](Types.Locale.md#zh_hans_ge)
- [zh\_Hans\_GF](Types.Locale.md#zh_hans_gf)
- [zh\_Hans\_GH](Types.Locale.md#zh_hans_gh)
- [zh\_Hans\_GI](Types.Locale.md#zh_hans_gi)
- [zh\_Hans\_GR](Types.Locale.md#zh_hans_gr)
- [zh\_Hans\_HK](Types.Locale.md#zh_hans_hk)
- [zh\_Hans\_HR](Types.Locale.md#zh_hans_hr)
- [zh\_Hans\_HU](Types.Locale.md#zh_hans_hu)
- [zh\_Hans\_ID](Types.Locale.md#zh_hans_id)
- [zh\_Hans\_IE](Types.Locale.md#zh_hans_ie)
- [zh\_Hans\_IL](Types.Locale.md#zh_hans_il)
- [zh\_Hans\_IN](Types.Locale.md#zh_hans_in)
- [zh\_Hans\_IS](Types.Locale.md#zh_hans_is)
- [zh\_Hans\_IT](Types.Locale.md#zh_hans_it)
- [zh\_Hans\_JO](Types.Locale.md#zh_hans_jo)
- [zh\_Hans\_JP](Types.Locale.md#zh_hans_jp)
- [zh\_Hans\_KR](Types.Locale.md#zh_hans_kr)
- [zh\_Hans\_KW](Types.Locale.md#zh_hans_kw)
- [zh\_Hans\_KY](Types.Locale.md#zh_hans_ky)
- [zh\_Hans\_LB](Types.Locale.md#zh_hans_lb)
- [zh\_Hans\_LI](Types.Locale.md#zh_hans_li)
- [zh\_Hans\_LT](Types.Locale.md#zh_hans_lt)
- [zh\_Hans\_LU](Types.Locale.md#zh_hans_lu)
- [zh\_Hans\_LV](Types.Locale.md#zh_hans_lv)
- [zh\_Hans\_MA](Types.Locale.md#zh_hans_ma)
- [zh\_Hans\_MC](Types.Locale.md#zh_hans_mc)
- [zh\_Hans\_MG](Types.Locale.md#zh_hans_mg)
- [zh\_Hans\_MK](Types.Locale.md#zh_hans_mk)
- [zh\_Hans\_MO](Types.Locale.md#zh_hans_mo)
- [zh\_Hans\_MT](Types.Locale.md#zh_hans_mt)
- [zh\_Hans\_MU](Types.Locale.md#zh_hans_mu)
- [zh\_Hans\_MX](Types.Locale.md#zh_hans_mx)
- [zh\_Hans\_MY](Types.Locale.md#zh_hans_my)
- [zh\_Hans\_NA](Types.Locale.md#zh_hans_na)
- [zh\_Hans\_NL](Types.Locale.md#zh_hans_nl)
- [zh\_Hans\_NO](Types.Locale.md#zh_hans_no)
- [zh\_Hans\_NZ](Types.Locale.md#zh_hans_nz)
- [zh\_Hans\_OM](Types.Locale.md#zh_hans_om)
- [zh\_Hans\_PE](Types.Locale.md#zh_hans_pe)
- [zh\_Hans\_PH](Types.Locale.md#zh_hans_ph)
- [zh\_Hans\_PK](Types.Locale.md#zh_hans_pk)
- [zh\_Hans\_PL](Types.Locale.md#zh_hans_pl)
- [zh\_Hans\_PR](Types.Locale.md#zh_hans_pr)
- [zh\_Hans\_PT](Types.Locale.md#zh_hans_pt)
- [zh\_Hans\_PY](Types.Locale.md#zh_hans_py)
- [zh\_Hans\_QA](Types.Locale.md#zh_hans_qa)
- [zh\_Hans\_RO](Types.Locale.md#zh_hans_ro)
- [zh\_Hans\_RU](Types.Locale.md#zh_hans_ru)
- [zh\_Hans\_SA](Types.Locale.md#zh_hans_sa)
- [zh\_Hans\_SE](Types.Locale.md#zh_hans_se)
- [zh\_Hans\_SG](Types.Locale.md#zh_hans_sg)
- [zh\_Hans\_SI](Types.Locale.md#zh_hans_si)
- [zh\_Hans\_SK](Types.Locale.md#zh_hans_sk)
- [zh\_Hans\_TH](Types.Locale.md#zh_hans_th)
- [zh\_Hans\_TR](Types.Locale.md#zh_hans_tr)
- [zh\_Hans\_TW](Types.Locale.md#zh_hans_tw)
- [zh\_Hans\_UA](Types.Locale.md#zh_hans_ua)
- [zh\_Hans\_US](Types.Locale.md#zh_hans_us)
- [zh\_Hans\_UY](Types.Locale.md#zh_hans_uy)
- [zh\_Hans\_VE](Types.Locale.md#zh_hans_ve)
- [zh\_Hans\_VN](Types.Locale.md#zh_hans_vn)
- [zh\_Hans\_ZA](Types.Locale.md#zh_hans_za)
- [zh\_Hant](Types.Locale.md#zh_hant)
- [zh\_Hant\_AE](Types.Locale.md#zh_hant_ae)
- [zh\_Hant\_AR](Types.Locale.md#zh_hant_ar)
- [zh\_Hant\_AT](Types.Locale.md#zh_hant_at)
- [zh\_Hant\_AU](Types.Locale.md#zh_hant_au)
- [zh\_Hant\_BE](Types.Locale.md#zh_hant_be)
- [zh\_Hant\_BG](Types.Locale.md#zh_hant_bg)
- [zh\_Hant\_BH](Types.Locale.md#zh_hant_bh)
- [zh\_Hant\_BR](Types.Locale.md#zh_hant_br)
- [zh\_Hant\_BW](Types.Locale.md#zh_hant_bw)
- [zh\_Hant\_CA](Types.Locale.md#zh_hant_ca)
- [zh\_Hant\_CH](Types.Locale.md#zh_hant_ch)
- [zh\_Hant\_CL](Types.Locale.md#zh_hant_cl)
- [zh\_Hant\_CN](Types.Locale.md#zh_hant_cn)
- [zh\_Hant\_CO](Types.Locale.md#zh_hant_co)
- [zh\_Hant\_CR](Types.Locale.md#zh_hant_cr)
- [zh\_Hant\_CY](Types.Locale.md#zh_hant_cy)
- [zh\_Hant\_CZ](Types.Locale.md#zh_hant_cz)
- [zh\_Hant\_DE](Types.Locale.md#zh_hant_de)
- [zh\_Hant\_DK](Types.Locale.md#zh_hant_dk)
- [zh\_Hant\_DO](Types.Locale.md#zh_hant_do)
- [zh\_Hant\_EE](Types.Locale.md#zh_hant_ee)
- [zh\_Hant\_EG](Types.Locale.md#zh_hant_eg)
- [zh\_Hant\_ES](Types.Locale.md#zh_hant_es)
- [zh\_Hant\_FI](Types.Locale.md#zh_hant_fi)
- [zh\_Hant\_GB](Types.Locale.md#zh_hant_gb)
- [zh\_Hant\_GE](Types.Locale.md#zh_hant_ge)
- [zh\_Hant\_GF](Types.Locale.md#zh_hant_gf)
- [zh\_Hant\_GH](Types.Locale.md#zh_hant_gh)
- [zh\_Hant\_GI](Types.Locale.md#zh_hant_gi)
- [zh\_Hant\_GR](Types.Locale.md#zh_hant_gr)
- [zh\_Hant\_HK](Types.Locale.md#zh_hant_hk)
- [zh\_Hant\_HR](Types.Locale.md#zh_hant_hr)
- [zh\_Hant\_HU](Types.Locale.md#zh_hant_hu)
- [zh\_Hant\_ID](Types.Locale.md#zh_hant_id)
- [zh\_Hant\_IE](Types.Locale.md#zh_hant_ie)
- [zh\_Hant\_IL](Types.Locale.md#zh_hant_il)
- [zh\_Hant\_IN](Types.Locale.md#zh_hant_in)
- [zh\_Hant\_IS](Types.Locale.md#zh_hant_is)
- [zh\_Hant\_IT](Types.Locale.md#zh_hant_it)
- [zh\_Hant\_JO](Types.Locale.md#zh_hant_jo)
- [zh\_Hant\_JP](Types.Locale.md#zh_hant_jp)
- [zh\_Hant\_KR](Types.Locale.md#zh_hant_kr)
- [zh\_Hant\_KW](Types.Locale.md#zh_hant_kw)
- [zh\_Hant\_KY](Types.Locale.md#zh_hant_ky)
- [zh\_Hant\_LB](Types.Locale.md#zh_hant_lb)
- [zh\_Hant\_LI](Types.Locale.md#zh_hant_li)
- [zh\_Hant\_LT](Types.Locale.md#zh_hant_lt)
- [zh\_Hant\_LU](Types.Locale.md#zh_hant_lu)
- [zh\_Hant\_LV](Types.Locale.md#zh_hant_lv)
- [zh\_Hant\_MA](Types.Locale.md#zh_hant_ma)
- [zh\_Hant\_MC](Types.Locale.md#zh_hant_mc)
- [zh\_Hant\_MG](Types.Locale.md#zh_hant_mg)
- [zh\_Hant\_MK](Types.Locale.md#zh_hant_mk)
- [zh\_Hant\_MO](Types.Locale.md#zh_hant_mo)
- [zh\_Hant\_MT](Types.Locale.md#zh_hant_mt)
- [zh\_Hant\_MU](Types.Locale.md#zh_hant_mu)
- [zh\_Hant\_MX](Types.Locale.md#zh_hant_mx)
- [zh\_Hant\_MY](Types.Locale.md#zh_hant_my)
- [zh\_Hant\_NA](Types.Locale.md#zh_hant_na)
- [zh\_Hant\_NL](Types.Locale.md#zh_hant_nl)
- [zh\_Hant\_NO](Types.Locale.md#zh_hant_no)
- [zh\_Hant\_NZ](Types.Locale.md#zh_hant_nz)
- [zh\_Hant\_OM](Types.Locale.md#zh_hant_om)
- [zh\_Hant\_PE](Types.Locale.md#zh_hant_pe)
- [zh\_Hant\_PH](Types.Locale.md#zh_hant_ph)
- [zh\_Hant\_PK](Types.Locale.md#zh_hant_pk)
- [zh\_Hant\_PL](Types.Locale.md#zh_hant_pl)
- [zh\_Hant\_PR](Types.Locale.md#zh_hant_pr)
- [zh\_Hant\_PT](Types.Locale.md#zh_hant_pt)
- [zh\_Hant\_PY](Types.Locale.md#zh_hant_py)
- [zh\_Hant\_QA](Types.Locale.md#zh_hant_qa)
- [zh\_Hant\_RO](Types.Locale.md#zh_hant_ro)
- [zh\_Hant\_RU](Types.Locale.md#zh_hant_ru)
- [zh\_Hant\_SA](Types.Locale.md#zh_hant_sa)
- [zh\_Hant\_SE](Types.Locale.md#zh_hant_se)
- [zh\_Hant\_SG](Types.Locale.md#zh_hant_sg)
- [zh\_Hant\_SI](Types.Locale.md#zh_hant_si)
- [zh\_Hant\_SK](Types.Locale.md#zh_hant_sk)
- [zh\_Hant\_TH](Types.Locale.md#zh_hant_th)
- [zh\_Hant\_TR](Types.Locale.md#zh_hant_tr)
- [zh\_Hant\_TW](Types.Locale.md#zh_hant_tw)
- [zh\_Hant\_UA](Types.Locale.md#zh_hant_ua)
- [zh\_Hant\_US](Types.Locale.md#zh_hant_us)
- [zh\_Hant\_UY](Types.Locale.md#zh_hant_uy)
- [zh\_Hant\_VE](Types.Locale.md#zh_hant_ve)
- [zh\_Hant\_VN](Types.Locale.md#zh_hant_vn)
- [zh\_Hant\_ZA](Types.Locale.md#zh_hant_za)
- [zu](Types.Locale.md#zu)
- [zu\_ZA](Types.Locale.md#zu_za)

## Enumeration Members

### af

• **af**

#### Defined in

[types/locale.ts:2](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L2)

___

### af\_NA

• **af\_NA**

#### Defined in

[types/locale.ts:3](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L3)

___

### af\_ZA

• **af\_ZA**

#### Defined in

[types/locale.ts:4](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L4)

___

### agq

• **agq**

#### Defined in

[types/locale.ts:5](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L5)

___

### agq\_CM

• **agq\_CM**

#### Defined in

[types/locale.ts:6](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L6)

___

### ak

• **ak**

#### Defined in

[types/locale.ts:7](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L7)

___

### ak\_GH

• **ak\_GH**

#### Defined in

[types/locale.ts:8](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L8)

___

### am

• **am**

#### Defined in

[types/locale.ts:9](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L9)

___

### am\_ET

• **am\_ET**

#### Defined in

[types/locale.ts:10](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L10)

___

### ar

• **ar**

#### Defined in

[types/locale.ts:11](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L11)

___

### ar\_001

• **ar\_001**

#### Defined in

[types/locale.ts:12](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L12)

___

### ar\_AE

• **ar\_AE**

#### Defined in

[types/locale.ts:13](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L13)

___

### ar\_BH

• **ar\_BH**

#### Defined in

[types/locale.ts:14](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L14)

___

### ar\_DZ

• **ar\_DZ**

#### Defined in

[types/locale.ts:15](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L15)

___

### ar\_EG

• **ar\_EG**

#### Defined in

[types/locale.ts:16](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L16)

___

### ar\_IQ

• **ar\_IQ**

#### Defined in

[types/locale.ts:17](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L17)

___

### ar\_JO

• **ar\_JO**

#### Defined in

[types/locale.ts:18](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L18)

___

### ar\_KW

• **ar\_KW**

#### Defined in

[types/locale.ts:19](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L19)

___

### ar\_LB

• **ar\_LB**

#### Defined in

[types/locale.ts:20](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L20)

___

### ar\_LY

• **ar\_LY**

#### Defined in

[types/locale.ts:21](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L21)

___

### ar\_MA

• **ar\_MA**

#### Defined in

[types/locale.ts:22](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L22)

___

### ar\_OM

• **ar\_OM**

#### Defined in

[types/locale.ts:23](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L23)

___

### ar\_QA

• **ar\_QA**

#### Defined in

[types/locale.ts:24](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L24)

___

### ar\_SA

• **ar\_SA**

#### Defined in

[types/locale.ts:25](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L25)

___

### ar\_SD

• **ar\_SD**

#### Defined in

[types/locale.ts:26](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L26)

___

### ar\_SY

• **ar\_SY**

#### Defined in

[types/locale.ts:27](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L27)

___

### ar\_TN

• **ar\_TN**

#### Defined in

[types/locale.ts:28](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L28)

___

### ar\_YE

• **ar\_YE**

#### Defined in

[types/locale.ts:29](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L29)

___

### as

• **as**

#### Defined in

[types/locale.ts:30](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L30)

___

### as\_IN

• **as\_IN**

#### Defined in

[types/locale.ts:31](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L31)

___

### asa

• **asa**

#### Defined in

[types/locale.ts:32](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L32)

___

### asa\_TZ

• **asa\_TZ**

#### Defined in

[types/locale.ts:33](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L33)

___

### az

• **az**

#### Defined in

[types/locale.ts:34](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L34)

___

### az\_Cyrl

• **az\_Cyrl**

#### Defined in

[types/locale.ts:35](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L35)

___

### az\_Cyrl\_AZ

• **az\_Cyrl\_AZ**

#### Defined in

[types/locale.ts:36](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L36)

___

### az\_Latn

• **az\_Latn**

#### Defined in

[types/locale.ts:37](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L37)

___

### az\_Latn\_AZ

• **az\_Latn\_AZ**

#### Defined in

[types/locale.ts:38](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L38)

___

### bas

• **bas**

#### Defined in

[types/locale.ts:39](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L39)

___

### bas\_CM

• **bas\_CM**

#### Defined in

[types/locale.ts:40](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L40)

___

### be

• **be**

#### Defined in

[types/locale.ts:41](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L41)

___

### be\_BY

• **be\_BY**

#### Defined in

[types/locale.ts:42](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L42)

___

### bem

• **bem**

#### Defined in

[types/locale.ts:43](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L43)

___

### bem\_ZM

• **bem\_ZM**

#### Defined in

[types/locale.ts:44](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L44)

___

### bez

• **bez**

#### Defined in

[types/locale.ts:45](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L45)

___

### bez\_TZ

• **bez\_TZ**

#### Defined in

[types/locale.ts:46](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L46)

___

### bg

• **bg**

#### Defined in

[types/locale.ts:47](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L47)

___

### bg\_BG

• **bg\_BG**

#### Defined in

[types/locale.ts:48](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L48)

___

### bm

• **bm**

#### Defined in

[types/locale.ts:49](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L49)

___

### bm\_ML

• **bm\_ML**

#### Defined in

[types/locale.ts:50](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L50)

___

### bn

• **bn**

#### Defined in

[types/locale.ts:51](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L51)

___

### bn\_BD

• **bn\_BD**

#### Defined in

[types/locale.ts:52](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L52)

___

### bn\_IN

• **bn\_IN**

#### Defined in

[types/locale.ts:53](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L53)

___

### bo

• **bo**

#### Defined in

[types/locale.ts:54](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L54)

___

### bo\_CN

• **bo\_CN**

#### Defined in

[types/locale.ts:55](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L55)

___

### bo\_IN

• **bo\_IN**

#### Defined in

[types/locale.ts:56](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L56)

___

### br

• **br**

#### Defined in

[types/locale.ts:57](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L57)

___

### br\_FR

• **br\_FR**

#### Defined in

[types/locale.ts:58](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L58)

___

### brx

• **brx**

#### Defined in

[types/locale.ts:59](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L59)

___

### brx\_IN

• **brx\_IN**

#### Defined in

[types/locale.ts:60](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L60)

___

### bs

• **bs**

#### Defined in

[types/locale.ts:61](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L61)

___

### bs\_BA

• **bs\_BA**

#### Defined in

[types/locale.ts:62](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L62)

___

### ca

• **ca**

#### Defined in

[types/locale.ts:63](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L63)

___

### ca\_ES

• **ca\_ES**

#### Defined in

[types/locale.ts:64](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L64)

___

### cgg

• **cgg**

#### Defined in

[types/locale.ts:65](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L65)

___

### cgg\_UG

• **cgg\_UG**

#### Defined in

[types/locale.ts:66](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L66)

___

### chr

• **chr**

#### Defined in

[types/locale.ts:67](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L67)

___

### chr\_US

• **chr\_US**

#### Defined in

[types/locale.ts:68](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L68)

___

### cs

• **cs**

#### Defined in

[types/locale.ts:69](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L69)

___

### cs\_CZ

• **cs\_CZ**

#### Defined in

[types/locale.ts:70](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L70)

___

### cy

• **cy**

#### Defined in

[types/locale.ts:71](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L71)

___

### cy\_GB

• **cy\_GB**

#### Defined in

[types/locale.ts:72](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L72)

___

### da

• **da**

#### Defined in

[types/locale.ts:73](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L73)

___

### da\_DK

• **da\_DK**

#### Defined in

[types/locale.ts:74](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L74)

___

### dav

• **dav**

#### Defined in

[types/locale.ts:75](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L75)

___

### dav\_KE

• **dav\_KE**

#### Defined in

[types/locale.ts:76](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L76)

___

### de

• **de**

#### Defined in

[types/locale.ts:77](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L77)

___

### de\_AT

• **de\_AT**

#### Defined in

[types/locale.ts:78](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L78)

___

### de\_BE

• **de\_BE**

#### Defined in

[types/locale.ts:79](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L79)

___

### de\_CH

• **de\_CH**

#### Defined in

[types/locale.ts:80](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L80)

___

### de\_DE

• **de\_DE**

#### Defined in

[types/locale.ts:81](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L81)

___

### de\_LI

• **de\_LI**

#### Defined in

[types/locale.ts:82](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L82)

___

### de\_LU

• **de\_LU**

#### Defined in

[types/locale.ts:83](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L83)

___

### dje

• **dje**

#### Defined in

[types/locale.ts:84](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L84)

___

### dje\_NE

• **dje\_NE**

#### Defined in

[types/locale.ts:85](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L85)

___

### dua

• **dua**

#### Defined in

[types/locale.ts:86](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L86)

___

### dua\_CM

• **dua\_CM**

#### Defined in

[types/locale.ts:87](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L87)

___

### dyo

• **dyo**

#### Defined in

[types/locale.ts:88](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L88)

___

### dyo\_SN

• **dyo\_SN**

#### Defined in

[types/locale.ts:89](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L89)

___

### ebu

• **ebu**

#### Defined in

[types/locale.ts:90](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L90)

___

### ebu\_KE

• **ebu\_KE**

#### Defined in

[types/locale.ts:91](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L91)

___

### ee

• **ee**

#### Defined in

[types/locale.ts:92](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L92)

___

### ee\_GH

• **ee\_GH**

#### Defined in

[types/locale.ts:93](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L93)

___

### ee\_TG

• **ee\_TG**

#### Defined in

[types/locale.ts:94](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L94)

___

### el

• **el**

#### Defined in

[types/locale.ts:95](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L95)

___

### el\_CY

• **el\_CY**

#### Defined in

[types/locale.ts:96](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L96)

___

### el\_GR

• **el\_GR**

#### Defined in

[types/locale.ts:97](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L97)

___

### en

• **en**

#### Defined in

[types/locale.ts:98](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L98)

___

### en\_AE

• **en\_AE**

#### Defined in

[types/locale.ts:99](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L99)

___

### en\_AR

• **en\_AR**

#### Defined in

[types/locale.ts:100](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L100)

___

### en\_AS

• **en\_AS**

#### Defined in

[types/locale.ts:101](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L101)

___

### en\_AT

• **en\_AT**

#### Defined in

[types/locale.ts:102](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L102)

___

### en\_AU

• **en\_AU**

#### Defined in

[types/locale.ts:103](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L103)

___

### en\_BB

• **en\_BB**

#### Defined in

[types/locale.ts:104](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L104)

___

### en\_BE

• **en\_BE**

#### Defined in

[types/locale.ts:105](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L105)

___

### en\_BG

• **en\_BG**

#### Defined in

[types/locale.ts:106](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L106)

___

### en\_BH

• **en\_BH**

#### Defined in

[types/locale.ts:107](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L107)

___

### en\_BM

• **en\_BM**

#### Defined in

[types/locale.ts:108](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L108)

___

### en\_BR

• **en\_BR**

#### Defined in

[types/locale.ts:109](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L109)

___

### en\_BW

• **en\_BW**

#### Defined in

[types/locale.ts:110](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L110)

___

### en\_BZ

• **en\_BZ**

#### Defined in

[types/locale.ts:111](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L111)

___

### en\_CA

• **en\_CA**

#### Defined in

[types/locale.ts:112](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L112)

___

### en\_CH

• **en\_CH**

#### Defined in

[types/locale.ts:113](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L113)

___

### en\_CL

• **en\_CL**

#### Defined in

[types/locale.ts:114](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L114)

___

### en\_CN

• **en\_CN**

#### Defined in

[types/locale.ts:115](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L115)

___

### en\_CO

• **en\_CO**

#### Defined in

[types/locale.ts:116](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L116)

___

### en\_CR

• **en\_CR**

#### Defined in

[types/locale.ts:117](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L117)

___

### en\_CY

• **en\_CY**

#### Defined in

[types/locale.ts:118](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L118)

___

### en\_CZ

• **en\_CZ**

#### Defined in

[types/locale.ts:119](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L119)

___

### en\_DE

• **en\_DE**

#### Defined in

[types/locale.ts:120](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L120)

___

### en\_DK

• **en\_DK**

#### Defined in

[types/locale.ts:121](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L121)

___

### en\_DO

• **en\_DO**

#### Defined in

[types/locale.ts:122](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L122)

___

### en\_EE

• **en\_EE**

#### Defined in

[types/locale.ts:123](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L123)

___

### en\_EG

• **en\_EG**

#### Defined in

[types/locale.ts:124](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L124)

___

### en\_ES

• **en\_ES**

#### Defined in

[types/locale.ts:125](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L125)

___

### en\_FI

• **en\_FI**

#### Defined in

[types/locale.ts:126](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L126)

___

### en\_GB

• **en\_GB**

#### Defined in

[types/locale.ts:127](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L127)

___

### en\_GE

• **en\_GE**

#### Defined in

[types/locale.ts:128](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L128)

___

### en\_GF

• **en\_GF**

#### Defined in

[types/locale.ts:129](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L129)

___

### en\_GH

• **en\_GH**

#### Defined in

[types/locale.ts:130](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L130)

___

### en\_GI

• **en\_GI**

#### Defined in

[types/locale.ts:131](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L131)

___

### en\_GR

• **en\_GR**

#### Defined in

[types/locale.ts:132](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L132)

___

### en\_GU

• **en\_GU**

#### Defined in

[types/locale.ts:133](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L133)

___

### en\_GY

• **en\_GY**

#### Defined in

[types/locale.ts:134](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L134)

___

### en\_HK

• **en\_HK**

#### Defined in

[types/locale.ts:135](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L135)

___

### en\_HR

• **en\_HR**

#### Defined in

[types/locale.ts:136](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L136)

___

### en\_HU

• **en\_HU**

#### Defined in

[types/locale.ts:137](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L137)

___

### en\_ID

• **en\_ID**

#### Defined in

[types/locale.ts:138](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L138)

___

### en\_IE

• **en\_IE**

#### Defined in

[types/locale.ts:139](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L139)

___

### en\_IL

• **en\_IL**

#### Defined in

[types/locale.ts:140](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L140)

___

### en\_IN

• **en\_IN**

#### Defined in

[types/locale.ts:141](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L141)

___

### en\_IS

• **en\_IS**

#### Defined in

[types/locale.ts:142](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L142)

___

### en\_IT

• **en\_IT**

#### Defined in

[types/locale.ts:143](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L143)

___

### en\_JM

• **en\_JM**

#### Defined in

[types/locale.ts:144](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L144)

___

### en\_JO

• **en\_JO**

#### Defined in

[types/locale.ts:145](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L145)

___

### en\_JP

• **en\_JP**

#### Defined in

[types/locale.ts:146](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L146)

___

### en\_KR

• **en\_KR**

#### Defined in

[types/locale.ts:147](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L147)

___

### en\_KW

• **en\_KW**

#### Defined in

[types/locale.ts:148](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L148)

___

### en\_KY

• **en\_KY**

#### Defined in

[types/locale.ts:149](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L149)

___

### en\_LB

• **en\_LB**

#### Defined in

[types/locale.ts:150](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L150)

___

### en\_LI

• **en\_LI**

#### Defined in

[types/locale.ts:151](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L151)

___

### en\_LT

• **en\_LT**

#### Defined in

[types/locale.ts:152](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L152)

___

### en\_LU

• **en\_LU**

#### Defined in

[types/locale.ts:153](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L153)

___

### en\_LV

• **en\_LV**

#### Defined in

[types/locale.ts:154](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L154)

___

### en\_MA

• **en\_MA**

#### Defined in

[types/locale.ts:155](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L155)

___

### en\_MC

• **en\_MC**

#### Defined in

[types/locale.ts:156](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L156)

___

### en\_MG

• **en\_MG**

#### Defined in

[types/locale.ts:157](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L157)

___

### en\_MH

• **en\_MH**

#### Defined in

[types/locale.ts:158](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L158)

___

### en\_MK

• **en\_MK**

#### Defined in

[types/locale.ts:159](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L159)

___

### en\_MO

• **en\_MO**

#### Defined in

[types/locale.ts:160](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L160)

___

### en\_MP

• **en\_MP**

#### Defined in

[types/locale.ts:161](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L161)

___

### en\_MT

• **en\_MT**

#### Defined in

[types/locale.ts:162](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L162)

___

### en\_MU

• **en\_MU**

#### Defined in

[types/locale.ts:163](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L163)

___

### en\_MX

• **en\_MX**

#### Defined in

[types/locale.ts:164](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L164)

___

### en\_MY

• **en\_MY**

#### Defined in

[types/locale.ts:165](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L165)

___

### en\_NA

• **en\_NA**

#### Defined in

[types/locale.ts:166](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L166)

___

### en\_NL

• **en\_NL**

#### Defined in

[types/locale.ts:167](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L167)

___

### en\_NO

• **en\_NO**

#### Defined in

[types/locale.ts:168](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L168)

___

### en\_NZ

• **en\_NZ**

#### Defined in

[types/locale.ts:169](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L169)

___

### en\_OM

• **en\_OM**

#### Defined in

[types/locale.ts:170](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L170)

___

### en\_PE

• **en\_PE**

#### Defined in

[types/locale.ts:171](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L171)

___

### en\_PH

• **en\_PH**

#### Defined in

[types/locale.ts:172](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L172)

___

### en\_PK

• **en\_PK**

#### Defined in

[types/locale.ts:173](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L173)

___

### en\_PL

• **en\_PL**

#### Defined in

[types/locale.ts:174](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L174)

___

### en\_PR

• **en\_PR**

#### Defined in

[types/locale.ts:175](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L175)

___

### en\_PT

• **en\_PT**

#### Defined in

[types/locale.ts:176](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L176)

___

### en\_PY

• **en\_PY**

#### Defined in

[types/locale.ts:177](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L177)

___

### en\_QA

• **en\_QA**

#### Defined in

[types/locale.ts:178](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L178)

___

### en\_RO

• **en\_RO**

#### Defined in

[types/locale.ts:179](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L179)

___

### en\_RU

• **en\_RU**

#### Defined in

[types/locale.ts:180](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L180)

___

### en\_SA

• **en\_SA**

#### Defined in

[types/locale.ts:181](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L181)

___

### en\_SE

• **en\_SE**

#### Defined in

[types/locale.ts:182](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L182)

___

### en\_SG

• **en\_SG**

#### Defined in

[types/locale.ts:183](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L183)

___

### en\_SI

• **en\_SI**

#### Defined in

[types/locale.ts:185](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L185)

___

### en\_SK

• **en\_SK**

#### Defined in

[types/locale.ts:184](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L184)

___

### en\_TH

• **en\_TH**

#### Defined in

[types/locale.ts:186](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L186)

___

### en\_TR

• **en\_TR**

#### Defined in

[types/locale.ts:187](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L187)

___

### en\_TT

• **en\_TT**

#### Defined in

[types/locale.ts:188](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L188)

___

### en\_TW

• **en\_TW**

#### Defined in

[types/locale.ts:189](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L189)

___

### en\_UA

• **en\_UA**

#### Defined in

[types/locale.ts:190](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L190)

___

### en\_UM

• **en\_UM**

#### Defined in

[types/locale.ts:191](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L191)

___

### en\_US

• **en\_US**

#### Defined in

[types/locale.ts:192](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L192)

___

### en\_US\_POSIX

• **en\_US\_POSIX**

#### Defined in

[types/locale.ts:193](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L193)

___

### en\_UY

• **en\_UY**

#### Defined in

[types/locale.ts:194](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L194)

___

### en\_VE

• **en\_VE**

#### Defined in

[types/locale.ts:195](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L195)

___

### en\_VI

• **en\_VI**

#### Defined in

[types/locale.ts:196](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L196)

___

### en\_VN

• **en\_VN**

#### Defined in

[types/locale.ts:197](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L197)

___

### en\_ZA

• **en\_ZA**

#### Defined in

[types/locale.ts:198](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L198)

___

### en\_ZW

• **en\_ZW**

#### Defined in

[types/locale.ts:199](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L199)

___

### eo

• **eo**

#### Defined in

[types/locale.ts:200](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L200)

___

### es

• **es**

#### Defined in

[types/locale.ts:201](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L201)

___

### es\_419

• **es\_419**

#### Defined in

[types/locale.ts:202](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L202)

___

### es\_AR

• **es\_AR**

#### Defined in

[types/locale.ts:203](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L203)

___

### es\_BO

• **es\_BO**

#### Defined in

[types/locale.ts:204](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L204)

___

### es\_CL

• **es\_CL**

#### Defined in

[types/locale.ts:205](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L205)

___

### es\_CO

• **es\_CO**

#### Defined in

[types/locale.ts:206](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L206)

___

### es\_CR

• **es\_CR**

#### Defined in

[types/locale.ts:207](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L207)

___

### es\_DO

• **es\_DO**

#### Defined in

[types/locale.ts:208](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L208)

___

### es\_EC

• **es\_EC**

#### Defined in

[types/locale.ts:209](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L209)

___

### es\_ES

• **es\_ES**

#### Defined in

[types/locale.ts:210](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L210)

___

### es\_GQ

• **es\_GQ**

#### Defined in

[types/locale.ts:211](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L211)

___

### es\_GT

• **es\_GT**

#### Defined in

[types/locale.ts:212](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L212)

___

### es\_HN

• **es\_HN**

#### Defined in

[types/locale.ts:213](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L213)

___

### es\_MX

• **es\_MX**

#### Defined in

[types/locale.ts:214](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L214)

___

### es\_NI

• **es\_NI**

#### Defined in

[types/locale.ts:215](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L215)

___

### es\_PA

• **es\_PA**

#### Defined in

[types/locale.ts:216](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L216)

___

### es\_PE

• **es\_PE**

#### Defined in

[types/locale.ts:217](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L217)

___

### es\_PR

• **es\_PR**

#### Defined in

[types/locale.ts:218](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L218)

___

### es\_PY

• **es\_PY**

#### Defined in

[types/locale.ts:219](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L219)

___

### es\_SV

• **es\_SV**

#### Defined in

[types/locale.ts:220](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L220)

___

### es\_US

• **es\_US**

#### Defined in

[types/locale.ts:221](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L221)

___

### es\_UY

• **es\_UY**

#### Defined in

[types/locale.ts:222](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L222)

___

### es\_VE

• **es\_VE**

#### Defined in

[types/locale.ts:223](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L223)

___

### et

• **et**

#### Defined in

[types/locale.ts:224](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L224)

___

### et\_EE

• **et\_EE**

#### Defined in

[types/locale.ts:225](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L225)

___

### eu

• **eu**

#### Defined in

[types/locale.ts:226](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L226)

___

### eu\_ES

• **eu\_ES**

#### Defined in

[types/locale.ts:227](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L227)

___

### ewo

• **ewo**

#### Defined in

[types/locale.ts:228](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L228)

___

### ewo\_CM

• **ewo\_CM**

#### Defined in

[types/locale.ts:229](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L229)

___

### fa

• **fa**

#### Defined in

[types/locale.ts:230](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L230)

___

### fa\_AF

• **fa\_AF**

#### Defined in

[types/locale.ts:231](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L231)

___

### fa\_IR

• **fa\_IR**

#### Defined in

[types/locale.ts:232](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L232)

___

### ff

• **ff**

#### Defined in

[types/locale.ts:233](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L233)

___

### ff\_SN

• **ff\_SN**

#### Defined in

[types/locale.ts:234](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L234)

___

### fi

• **fi**

#### Defined in

[types/locale.ts:235](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L235)

___

### fi\_FI

• **fi\_FI**

#### Defined in

[types/locale.ts:236](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L236)

___

### fil

• **fil**

#### Defined in

[types/locale.ts:237](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L237)

___

### fil\_PH

• **fil\_PH**

#### Defined in

[types/locale.ts:238](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L238)

___

### fo

• **fo**

#### Defined in

[types/locale.ts:239](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L239)

___

### fo\_FO

• **fo\_FO**

#### Defined in

[types/locale.ts:240](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L240)

___

### fr

• **fr**

#### Defined in

[types/locale.ts:241](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L241)

___

### fr\_BE

• **fr\_BE**

#### Defined in

[types/locale.ts:242](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L242)

___

### fr\_BF

• **fr\_BF**

#### Defined in

[types/locale.ts:243](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L243)

___

### fr\_BI

• **fr\_BI**

#### Defined in

[types/locale.ts:244](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L244)

___

### fr\_BJ

• **fr\_BJ**

#### Defined in

[types/locale.ts:245](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L245)

___

### fr\_BL

• **fr\_BL**

#### Defined in

[types/locale.ts:246](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L246)

___

### fr\_CA

• **fr\_CA**

#### Defined in

[types/locale.ts:247](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L247)

___

### fr\_CD

• **fr\_CD**

#### Defined in

[types/locale.ts:248](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L248)

___

### fr\_CF

• **fr\_CF**

#### Defined in

[types/locale.ts:249](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L249)

___

### fr\_CG

• **fr\_CG**

#### Defined in

[types/locale.ts:250](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L250)

___

### fr\_CH

• **fr\_CH**

#### Defined in

[types/locale.ts:251](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L251)

___

### fr\_CI

• **fr\_CI**

#### Defined in

[types/locale.ts:252](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L252)

___

### fr\_CM

• **fr\_CM**

#### Defined in

[types/locale.ts:253](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L253)

___

### fr\_DJ

• **fr\_DJ**

#### Defined in

[types/locale.ts:254](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L254)

___

### fr\_FR

• **fr\_FR**

#### Defined in

[types/locale.ts:255](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L255)

___

### fr\_GA

• **fr\_GA**

#### Defined in

[types/locale.ts:256](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L256)

___

### fr\_GF

• **fr\_GF**

#### Defined in

[types/locale.ts:257](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L257)

___

### fr\_GN

• **fr\_GN**

#### Defined in

[types/locale.ts:258](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L258)

___

### fr\_GP

• **fr\_GP**

#### Defined in

[types/locale.ts:259](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L259)

___

### fr\_GQ

• **fr\_GQ**

#### Defined in

[types/locale.ts:260](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L260)

___

### fr\_KM

• **fr\_KM**

#### Defined in

[types/locale.ts:261](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L261)

___

### fr\_LU

• **fr\_LU**

#### Defined in

[types/locale.ts:262](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L262)

___

### fr\_MC

• **fr\_MC**

#### Defined in

[types/locale.ts:263](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L263)

___

### fr\_MF

• **fr\_MF**

#### Defined in

[types/locale.ts:264](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L264)

___

### fr\_MG

• **fr\_MG**

#### Defined in

[types/locale.ts:265](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L265)

___

### fr\_ML

• **fr\_ML**

#### Defined in

[types/locale.ts:266](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L266)

___

### fr\_MQ

• **fr\_MQ**

#### Defined in

[types/locale.ts:267](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L267)

___

### fr\_NE

• **fr\_NE**

#### Defined in

[types/locale.ts:268](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L268)

___

### fr\_RE

• **fr\_RE**

#### Defined in

[types/locale.ts:269](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L269)

___

### fr\_RW

• **fr\_RW**

#### Defined in

[types/locale.ts:270](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L270)

___

### fr\_SN

• **fr\_SN**

#### Defined in

[types/locale.ts:271](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L271)

___

### fr\_TD

• **fr\_TD**

#### Defined in

[types/locale.ts:272](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L272)

___

### fr\_TG

• **fr\_TG**

#### Defined in

[types/locale.ts:273](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L273)

___

### fr\_YT

• **fr\_YT**

#### Defined in

[types/locale.ts:274](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L274)

___

### ga

• **ga**

#### Defined in

[types/locale.ts:275](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L275)

___

### ga\_IE

• **ga\_IE**

#### Defined in

[types/locale.ts:276](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L276)

___

### gl

• **gl**

#### Defined in

[types/locale.ts:277](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L277)

___

### gl\_ES

• **gl\_ES**

#### Defined in

[types/locale.ts:278](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L278)

___

### gsw

• **gsw**

#### Defined in

[types/locale.ts:279](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L279)

___

### gsw\_CH

• **gsw\_CH**

#### Defined in

[types/locale.ts:280](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L280)

___

### gu

• **gu**

#### Defined in

[types/locale.ts:281](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L281)

___

### gu\_IN

• **gu\_IN**

#### Defined in

[types/locale.ts:282](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L282)

___

### guz

• **guz**

#### Defined in

[types/locale.ts:283](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L283)

___

### guz\_KE

• **guz\_KE**

#### Defined in

[types/locale.ts:284](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L284)

___

### gv

• **gv**

#### Defined in

[types/locale.ts:285](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L285)

___

### gv\_GB

• **gv\_GB**

#### Defined in

[types/locale.ts:286](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L286)

___

### ha

• **ha**

#### Defined in

[types/locale.ts:287](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L287)

___

### ha\_Latn

• **ha\_Latn**

#### Defined in

[types/locale.ts:288](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L288)

___

### ha\_Latn\_GH

• **ha\_Latn\_GH**

#### Defined in

[types/locale.ts:289](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L289)

___

### ha\_Latn\_NE

• **ha\_Latn\_NE**

#### Defined in

[types/locale.ts:290](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L290)

___

### ha\_Latn\_NG

• **ha\_Latn\_NG**

#### Defined in

[types/locale.ts:291](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L291)

___

### haw

• **haw**

#### Defined in

[types/locale.ts:292](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L292)

___

### haw\_US

• **haw\_US**

#### Defined in

[types/locale.ts:293](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L293)

___

### he

• **he**

#### Defined in

[types/locale.ts:294](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L294)

___

### he\_IL

• **he\_IL**

#### Defined in

[types/locale.ts:295](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L295)

___

### hi

• **hi**

#### Defined in

[types/locale.ts:296](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L296)

___

### hi\_IN

• **hi\_IN**

#### Defined in

[types/locale.ts:297](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L297)

___

### hr

• **hr**

#### Defined in

[types/locale.ts:298](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L298)

___

### hr\_HR

• **hr\_HR**

#### Defined in

[types/locale.ts:299](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L299)

___

### hu

• **hu**

#### Defined in

[types/locale.ts:300](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L300)

___

### hu\_HU

• **hu\_HU**

#### Defined in

[types/locale.ts:301](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L301)

___

### hy

• **hy**

#### Defined in

[types/locale.ts:302](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L302)

___

### hy\_AM

• **hy\_AM**

#### Defined in

[types/locale.ts:303](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L303)

___

### id

• **id**

#### Defined in

[types/locale.ts:304](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L304)

___

### id\_ID

• **id\_ID**

#### Defined in

[types/locale.ts:305](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L305)

___

### ig

• **ig**

#### Defined in

[types/locale.ts:306](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L306)

___

### ig\_NG

• **ig\_NG**

#### Defined in

[types/locale.ts:307](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L307)

___

### ii

• **ii**

#### Defined in

[types/locale.ts:308](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L308)

___

### ii\_CN

• **ii\_CN**

#### Defined in

[types/locale.ts:309](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L309)

___

### is

• **is**

#### Defined in

[types/locale.ts:310](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L310)

___

### is\_IS

• **is\_IS**

#### Defined in

[types/locale.ts:311](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L311)

___

### it

• **it**

#### Defined in

[types/locale.ts:312](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L312)

___

### it\_CH

• **it\_CH**

#### Defined in

[types/locale.ts:313](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L313)

___

### it\_IT

• **it\_IT**

#### Defined in

[types/locale.ts:314](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L314)

___

### ja

• **ja**

#### Defined in

[types/locale.ts:315](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L315)

___

### ja\_JP

• **ja\_JP**

#### Defined in

[types/locale.ts:316](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L316)

___

### jmc

• **jmc**

#### Defined in

[types/locale.ts:317](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L317)

___

### jmc\_TZ

• **jmc\_TZ**

#### Defined in

[types/locale.ts:318](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L318)

___

### ka

• **ka**

#### Defined in

[types/locale.ts:319](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L319)

___

### ka\_GE

• **ka\_GE**

#### Defined in

[types/locale.ts:320](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L320)

___

### kab

• **kab**

#### Defined in

[types/locale.ts:321](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L321)

___

### kab\_DZ

• **kab\_DZ**

#### Defined in

[types/locale.ts:322](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L322)

___

### kam

• **kam**

#### Defined in

[types/locale.ts:323](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L323)

___

### kam\_KE

• **kam\_KE**

#### Defined in

[types/locale.ts:324](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L324)

___

### kde

• **kde**

#### Defined in

[types/locale.ts:325](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L325)

___

### kde\_TZ

• **kde\_TZ**

#### Defined in

[types/locale.ts:326](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L326)

___

### kea

• **kea**

#### Defined in

[types/locale.ts:327](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L327)

___

### kea\_CV

• **kea\_CV**

#### Defined in

[types/locale.ts:328](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L328)

___

### khq

• **khq**

#### Defined in

[types/locale.ts:329](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L329)

___

### khq\_ML

• **khq\_ML**

#### Defined in

[types/locale.ts:330](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L330)

___

### ki

• **ki**

#### Defined in

[types/locale.ts:331](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L331)

___

### ki\_KE

• **ki\_KE**

#### Defined in

[types/locale.ts:332](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L332)

___

### kk

• **kk**

#### Defined in

[types/locale.ts:333](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L333)

___

### kk\_Cyrl

• **kk\_Cyrl**

#### Defined in

[types/locale.ts:334](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L334)

___

### kk\_Cyrl\_KZ

• **kk\_Cyrl\_KZ**

#### Defined in

[types/locale.ts:335](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L335)

___

### kl

• **kl**

#### Defined in

[types/locale.ts:336](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L336)

___

### kl\_GL

• **kl\_GL**

#### Defined in

[types/locale.ts:337](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L337)

___

### kln

• **kln**

#### Defined in

[types/locale.ts:338](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L338)

___

### kln\_KE

• **kln\_KE**

#### Defined in

[types/locale.ts:339](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L339)

___

### km

• **km**

#### Defined in

[types/locale.ts:340](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L340)

___

### km\_KH

• **km\_KH**

#### Defined in

[types/locale.ts:341](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L341)

___

### kn

• **kn**

#### Defined in

[types/locale.ts:342](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L342)

___

### kn\_IN

• **kn\_IN**

#### Defined in

[types/locale.ts:343](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L343)

___

### ko

• **ko**

#### Defined in

[types/locale.ts:344](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L344)

___

### ko\_KR

• **ko\_KR**

#### Defined in

[types/locale.ts:345](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L345)

___

### kok

• **kok**

#### Defined in

[types/locale.ts:346](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L346)

___

### kok\_IN

• **kok\_IN**

#### Defined in

[types/locale.ts:347](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L347)

___

### ksb

• **ksb**

#### Defined in

[types/locale.ts:348](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L348)

___

### ksb\_TZ

• **ksb\_TZ**

#### Defined in

[types/locale.ts:349](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L349)

___

### ksf

• **ksf**

#### Defined in

[types/locale.ts:350](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L350)

___

### ksf\_CM

• **ksf\_CM**

#### Defined in

[types/locale.ts:351](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L351)

___

### kw

• **kw**

#### Defined in

[types/locale.ts:352](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L352)

___

### kw\_GB

• **kw\_GB**

#### Defined in

[types/locale.ts:353](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L353)

___

### lag

• **lag**

#### Defined in

[types/locale.ts:354](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L354)

___

### lag\_TZ

• **lag\_TZ**

#### Defined in

[types/locale.ts:355](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L355)

___

### lg

• **lg**

#### Defined in

[types/locale.ts:356](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L356)

___

### lg\_UG

• **lg\_UG**

#### Defined in

[types/locale.ts:357](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L357)

___

### ln

• **ln**

#### Defined in

[types/locale.ts:358](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L358)

___

### ln\_CD

• **ln\_CD**

#### Defined in

[types/locale.ts:359](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L359)

___

### ln\_CG

• **ln\_CG**

#### Defined in

[types/locale.ts:360](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L360)

___

### lt

• **lt**

#### Defined in

[types/locale.ts:361](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L361)

___

### lt\_LT

• **lt\_LT**

#### Defined in

[types/locale.ts:362](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L362)

___

### lu

• **lu**

#### Defined in

[types/locale.ts:363](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L363)

___

### lu\_CD

• **lu\_CD**

#### Defined in

[types/locale.ts:364](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L364)

___

### luo

• **luo**

#### Defined in

[types/locale.ts:365](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L365)

___

### luo\_KE

• **luo\_KE**

#### Defined in

[types/locale.ts:366](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L366)

___

### luy

• **luy**

#### Defined in

[types/locale.ts:367](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L367)

___

### luy\_KE

• **luy\_KE**

#### Defined in

[types/locale.ts:368](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L368)

___

### lv

• **lv**

#### Defined in

[types/locale.ts:369](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L369)

___

### lv\_LV

• **lv\_LV**

#### Defined in

[types/locale.ts:370](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L370)

___

### mas

• **mas**

#### Defined in

[types/locale.ts:371](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L371)

___

### mas\_KE

• **mas\_KE**

#### Defined in

[types/locale.ts:372](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L372)

___

### mas\_TZ

• **mas\_TZ**

#### Defined in

[types/locale.ts:373](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L373)

___

### mer

• **mer**

#### Defined in

[types/locale.ts:374](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L374)

___

### mer\_KE

• **mer\_KE**

#### Defined in

[types/locale.ts:375](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L375)

___

### mfe

• **mfe**

#### Defined in

[types/locale.ts:376](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L376)

___

### mfe\_MU

• **mfe\_MU**

#### Defined in

[types/locale.ts:377](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L377)

___

### mg

• **mg**

#### Defined in

[types/locale.ts:378](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L378)

___

### mg\_MG

• **mg\_MG**

#### Defined in

[types/locale.ts:379](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L379)

___

### mgh

• **mgh**

#### Defined in

[types/locale.ts:380](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L380)

___

### mgh\_MZ

• **mgh\_MZ**

#### Defined in

[types/locale.ts:381](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L381)

___

### mk

• **mk**

#### Defined in

[types/locale.ts:382](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L382)

___

### mk\_MK

• **mk\_MK**

#### Defined in

[types/locale.ts:383](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L383)

___

### ml

• **ml**

#### Defined in

[types/locale.ts:384](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L384)

___

### ml\_IN

• **ml\_IN**

#### Defined in

[types/locale.ts:385](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L385)

___

### mr

• **mr**

#### Defined in

[types/locale.ts:386](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L386)

___

### mr\_IN

• **mr\_IN**

#### Defined in

[types/locale.ts:387](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L387)

___

### ms

• **ms**

#### Defined in

[types/locale.ts:388](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L388)

___

### ms\_BN

• **ms\_BN**

#### Defined in

[types/locale.ts:389](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L389)

___

### ms\_MY

• **ms\_MY**

#### Defined in

[types/locale.ts:390](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L390)

___

### mt

• **mt**

#### Defined in

[types/locale.ts:391](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L391)

___

### mt\_MT

• **mt\_MT**

#### Defined in

[types/locale.ts:392](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L392)

___

### mua

• **mua**

#### Defined in

[types/locale.ts:393](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L393)

___

### mua\_CM

• **mua\_CM**

#### Defined in

[types/locale.ts:394](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L394)

___

### my

• **my**

#### Defined in

[types/locale.ts:395](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L395)

___

### my\_MM

• **my\_MM**

#### Defined in

[types/locale.ts:396](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L396)

___

### naq

• **naq**

#### Defined in

[types/locale.ts:397](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L397)

___

### naq\_NA

• **naq\_NA**

#### Defined in

[types/locale.ts:398](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L398)

___

### nb

• **nb**

#### Defined in

[types/locale.ts:399](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L399)

___

### nb\_NO

• **nb\_NO**

#### Defined in

[types/locale.ts:400](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L400)

___

### nd

• **nd**

#### Defined in

[types/locale.ts:401](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L401)

___

### nd\_ZW

• **nd\_ZW**

#### Defined in

[types/locale.ts:402](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L402)

___

### ne

• **ne**

#### Defined in

[types/locale.ts:403](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L403)

___

### ne\_IN

• **ne\_IN**

#### Defined in

[types/locale.ts:404](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L404)

___

### ne\_NP

• **ne\_NP**

#### Defined in

[types/locale.ts:405](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L405)

___

### nl

• **nl**

#### Defined in

[types/locale.ts:406](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L406)

___

### nl\_AW

• **nl\_AW**

#### Defined in

[types/locale.ts:407](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L407)

___

### nl\_BE

• **nl\_BE**

#### Defined in

[types/locale.ts:408](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L408)

___

### nl\_CW

• **nl\_CW**

#### Defined in

[types/locale.ts:409](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L409)

___

### nl\_NL

• **nl\_NL**

#### Defined in

[types/locale.ts:410](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L410)

___

### nl\_SX

• **nl\_SX**

#### Defined in

[types/locale.ts:411](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L411)

___

### nmg

• **nmg**

#### Defined in

[types/locale.ts:412](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L412)

___

### nmg\_CM

• **nmg\_CM**

#### Defined in

[types/locale.ts:413](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L413)

___

### nn

• **nn**

#### Defined in

[types/locale.ts:414](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L414)

___

### nn\_NO

• **nn\_NO**

#### Defined in

[types/locale.ts:415](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L415)

___

### nus

• **nus**

#### Defined in

[types/locale.ts:416](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L416)

___

### nus\_SD

• **nus\_SD**

#### Defined in

[types/locale.ts:417](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L417)

___

### nyn

• **nyn**

#### Defined in

[types/locale.ts:418](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L418)

___

### nyn\_UG

• **nyn\_UG**

#### Defined in

[types/locale.ts:419](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L419)

___

### om

• **om**

#### Defined in

[types/locale.ts:420](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L420)

___

### om\_ET

• **om\_ET**

#### Defined in

[types/locale.ts:421](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L421)

___

### om\_KE

• **om\_KE**

#### Defined in

[types/locale.ts:422](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L422)

___

### or

• **or**

#### Defined in

[types/locale.ts:423](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L423)

___

### or\_IN

• **or\_IN**

#### Defined in

[types/locale.ts:424](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L424)

___

### pa

• **pa**

#### Defined in

[types/locale.ts:425](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L425)

___

### pa\_Arab

• **pa\_Arab**

#### Defined in

[types/locale.ts:426](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L426)

___

### pa\_Arab\_PK

• **pa\_Arab\_PK**

#### Defined in

[types/locale.ts:427](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L427)

___

### pa\_Guru

• **pa\_Guru**

#### Defined in

[types/locale.ts:428](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L428)

___

### pa\_Guru\_IN

• **pa\_Guru\_IN**

#### Defined in

[types/locale.ts:429](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L429)

___

### pl

• **pl**

#### Defined in

[types/locale.ts:430](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L430)

___

### pl\_PL

• **pl\_PL**

#### Defined in

[types/locale.ts:431](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L431)

___

### ps

• **ps**

#### Defined in

[types/locale.ts:432](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L432)

___

### ps\_AF

• **ps\_AF**

#### Defined in

[types/locale.ts:433](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L433)

___

### pt

• **pt**

#### Defined in

[types/locale.ts:434](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L434)

___

### pt\_AO

• **pt\_AO**

#### Defined in

[types/locale.ts:435](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L435)

___

### pt\_BR

• **pt\_BR**

#### Defined in

[types/locale.ts:436](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L436)

___

### pt\_GW

• **pt\_GW**

#### Defined in

[types/locale.ts:437](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L437)

___

### pt\_MZ

• **pt\_MZ**

#### Defined in

[types/locale.ts:438](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L438)

___

### pt\_PT

• **pt\_PT**

#### Defined in

[types/locale.ts:439](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L439)

___

### pt\_ST

• **pt\_ST**

#### Defined in

[types/locale.ts:440](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L440)

___

### rm

• **rm**

#### Defined in

[types/locale.ts:441](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L441)

___

### rm\_CH

• **rm\_CH**

#### Defined in

[types/locale.ts:442](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L442)

___

### rn

• **rn**

#### Defined in

[types/locale.ts:443](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L443)

___

### rn\_BI

• **rn\_BI**

#### Defined in

[types/locale.ts:444](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L444)

___

### ro

• **ro**

#### Defined in

[types/locale.ts:445](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L445)

___

### ro\_MD

• **ro\_MD**

#### Defined in

[types/locale.ts:446](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L446)

___

### ro\_RO

• **ro\_RO**

#### Defined in

[types/locale.ts:447](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L447)

___

### rof

• **rof**

#### Defined in

[types/locale.ts:448](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L448)

___

### rof\_TZ

• **rof\_TZ**

#### Defined in

[types/locale.ts:449](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L449)

___

### ru

• **ru**

#### Defined in

[types/locale.ts:450](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L450)

___

### ru\_MD

• **ru\_MD**

#### Defined in

[types/locale.ts:451](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L451)

___

### ru\_RU

• **ru\_RU**

#### Defined in

[types/locale.ts:452](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L452)

___

### ru\_UA

• **ru\_UA**

#### Defined in

[types/locale.ts:453](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L453)

___

### rw

• **rw**

#### Defined in

[types/locale.ts:454](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L454)

___

### rw\_RW

• **rw\_RW**

#### Defined in

[types/locale.ts:455](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L455)

___

### rwk

• **rwk**

#### Defined in

[types/locale.ts:456](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L456)

___

### rwk\_TZ

• **rwk\_TZ**

#### Defined in

[types/locale.ts:457](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L457)

___

### saq

• **saq**

#### Defined in

[types/locale.ts:458](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L458)

___

### saq\_KE

• **saq\_KE**

#### Defined in

[types/locale.ts:459](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L459)

___

### sbp

• **sbp**

#### Defined in

[types/locale.ts:460](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L460)

___

### sbp\_TZ

• **sbp\_TZ**

#### Defined in

[types/locale.ts:461](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L461)

___

### seh

• **seh**

#### Defined in

[types/locale.ts:462](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L462)

___

### seh\_MZ

• **seh\_MZ**

#### Defined in

[types/locale.ts:463](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L463)

___

### ses

• **ses**

#### Defined in

[types/locale.ts:464](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L464)

___

### ses\_ML

• **ses\_ML**

#### Defined in

[types/locale.ts:465](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L465)

___

### sg

• **sg**

#### Defined in

[types/locale.ts:466](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L466)

___

### sg\_CF

• **sg\_CF**

#### Defined in

[types/locale.ts:467](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L467)

___

### shi

• **shi**

#### Defined in

[types/locale.ts:468](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L468)

___

### shi\_Latn

• **shi\_Latn**

#### Defined in

[types/locale.ts:469](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L469)

___

### shi\_Latn\_MA

• **shi\_Latn\_MA**

#### Defined in

[types/locale.ts:470](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L470)

___

### shi\_Tfng

• **shi\_Tfng**

#### Defined in

[types/locale.ts:471](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L471)

___

### shi\_Tfng\_MA

• **shi\_Tfng\_MA**

#### Defined in

[types/locale.ts:472](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L472)

___

### si

• **si**

#### Defined in

[types/locale.ts:473](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L473)

___

### si\_LK

• **si\_LK**

#### Defined in

[types/locale.ts:474](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L474)

___

### sk

• **sk**

#### Defined in

[types/locale.ts:475](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L475)

___

### sk\_SK

• **sk\_SK**

#### Defined in

[types/locale.ts:476](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L476)

___

### sl

• **sl**

#### Defined in

[types/locale.ts:477](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L477)

___

### sl\_SI

• **sl\_SI**

#### Defined in

[types/locale.ts:478](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L478)

___

### sn

• **sn**

#### Defined in

[types/locale.ts:479](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L479)

___

### sn\_ZW

• **sn\_ZW**

#### Defined in

[types/locale.ts:480](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L480)

___

### so

• **so**

#### Defined in

[types/locale.ts:481](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L481)

___

### so\_DJ

• **so\_DJ**

#### Defined in

[types/locale.ts:482](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L482)

___

### so\_ET

• **so\_ET**

#### Defined in

[types/locale.ts:483](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L483)

___

### so\_KE

• **so\_KE**

#### Defined in

[types/locale.ts:484](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L484)

___

### so\_SO

• **so\_SO**

#### Defined in

[types/locale.ts:485](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L485)

___

### sq

• **sq**

#### Defined in

[types/locale.ts:486](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L486)

___

### sq\_AL

• **sq\_AL**

#### Defined in

[types/locale.ts:487](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L487)

___

### sr

• **sr**

#### Defined in

[types/locale.ts:488](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L488)

___

### sr\_Cyrl

• **sr\_Cyrl**

#### Defined in

[types/locale.ts:489](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L489)

___

### sr\_Cyrl\_BA

• **sr\_Cyrl\_BA**

#### Defined in

[types/locale.ts:490](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L490)

___

### sr\_Cyrl\_ME

• **sr\_Cyrl\_ME**

#### Defined in

[types/locale.ts:491](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L491)

___

### sr\_Cyrl\_RS

• **sr\_Cyrl\_RS**

#### Defined in

[types/locale.ts:492](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L492)

___

### sr\_Latn

• **sr\_Latn**

#### Defined in

[types/locale.ts:493](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L493)

___

### sr\_Latn\_BA

• **sr\_Latn\_BA**

#### Defined in

[types/locale.ts:494](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L494)

___

### sr\_Latn\_ME

• **sr\_Latn\_ME**

#### Defined in

[types/locale.ts:495](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L495)

___

### sr\_Latn\_RS

• **sr\_Latn\_RS**

#### Defined in

[types/locale.ts:496](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L496)

___

### sv

• **sv**

#### Defined in

[types/locale.ts:497](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L497)

___

### sv\_FI

• **sv\_FI**

#### Defined in

[types/locale.ts:498](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L498)

___

### sv\_SE

• **sv\_SE**

#### Defined in

[types/locale.ts:499](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L499)

___

### sw

• **sw**

#### Defined in

[types/locale.ts:500](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L500)

___

### sw\_KE

• **sw\_KE**

#### Defined in

[types/locale.ts:501](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L501)

___

### sw\_TZ

• **sw\_TZ**

#### Defined in

[types/locale.ts:502](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L502)

___

### swc

• **swc**

#### Defined in

[types/locale.ts:503](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L503)

___

### swc\_CD

• **swc\_CD**

#### Defined in

[types/locale.ts:504](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L504)

___

### ta

• **ta**

#### Defined in

[types/locale.ts:505](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L505)

___

### ta\_IN

• **ta\_IN**

#### Defined in

[types/locale.ts:506](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L506)

___

### ta\_LK

• **ta\_LK**

#### Defined in

[types/locale.ts:507](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L507)

___

### te

• **te**

#### Defined in

[types/locale.ts:508](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L508)

___

### te\_IN

• **te\_IN**

#### Defined in

[types/locale.ts:509](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L509)

___

### teo

• **teo**

#### Defined in

[types/locale.ts:510](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L510)

___

### teo\_KE

• **teo\_KE**

#### Defined in

[types/locale.ts:511](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L511)

___

### teo\_UG

• **teo\_UG**

#### Defined in

[types/locale.ts:512](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L512)

___

### th

• **th**

#### Defined in

[types/locale.ts:513](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L513)

___

### th\_TH

• **th\_TH**

#### Defined in

[types/locale.ts:514](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L514)

___

### ti

• **ti**

#### Defined in

[types/locale.ts:515](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L515)

___

### ti\_ER

• **ti\_ER**

#### Defined in

[types/locale.ts:516](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L516)

___

### ti\_ET

• **ti\_ET**

#### Defined in

[types/locale.ts:517](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L517)

___

### to

• **to**

#### Defined in

[types/locale.ts:518](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L518)

___

### to\_TO

• **to\_TO**

#### Defined in

[types/locale.ts:519](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L519)

___

### tr

• **tr**

#### Defined in

[types/locale.ts:520](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L520)

___

### tr\_TR

• **tr\_TR**

#### Defined in

[types/locale.ts:521](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L521)

___

### twq

• **twq**

#### Defined in

[types/locale.ts:522](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L522)

___

### twq\_NE

• **twq\_NE**

#### Defined in

[types/locale.ts:523](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L523)

___

### tzm

• **tzm**

#### Defined in

[types/locale.ts:524](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L524)

___

### tzm\_Latn

• **tzm\_Latn**

#### Defined in

[types/locale.ts:525](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L525)

___

### tzm\_Latn\_MA

• **tzm\_Latn\_MA**

#### Defined in

[types/locale.ts:526](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L526)

___

### uk

• **uk**

#### Defined in

[types/locale.ts:527](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L527)

___

### uk\_UA

• **uk\_UA**

#### Defined in

[types/locale.ts:528](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L528)

___

### ur

• **ur**

#### Defined in

[types/locale.ts:529](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L529)

___

### ur\_IN

• **ur\_IN**

#### Defined in

[types/locale.ts:530](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L530)

___

### ur\_PK

• **ur\_PK**

#### Defined in

[types/locale.ts:531](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L531)

___

### uz

• **uz**

#### Defined in

[types/locale.ts:532](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L532)

___

### uz\_Arab

• **uz\_Arab**

#### Defined in

[types/locale.ts:533](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L533)

___

### uz\_Arab\_AF

• **uz\_Arab\_AF**

#### Defined in

[types/locale.ts:534](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L534)

___

### uz\_Cyrl

• **uz\_Cyrl**

#### Defined in

[types/locale.ts:535](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L535)

___

### uz\_Cyrl\_UZ

• **uz\_Cyrl\_UZ**

#### Defined in

[types/locale.ts:536](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L536)

___

### uz\_Latn

• **uz\_Latn**

#### Defined in

[types/locale.ts:537](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L537)

___

### uz\_Latn\_UZ

• **uz\_Latn\_UZ**

#### Defined in

[types/locale.ts:538](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L538)

___

### vai

• **vai**

#### Defined in

[types/locale.ts:539](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L539)

___

### vai\_Latn

• **vai\_Latn**

#### Defined in

[types/locale.ts:540](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L540)

___

### vai\_Latn\_LR

• **vai\_Latn\_LR**

#### Defined in

[types/locale.ts:541](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L541)

___

### vai\_Vaii

• **vai\_Vaii**

#### Defined in

[types/locale.ts:542](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L542)

___

### vai\_Vaii\_LR

• **vai\_Vaii\_LR**

#### Defined in

[types/locale.ts:543](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L543)

___

### vi

• **vi**

#### Defined in

[types/locale.ts:544](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L544)

___

### vi\_VN

• **vi\_VN**

#### Defined in

[types/locale.ts:545](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L545)

___

### vun

• **vun**

#### Defined in

[types/locale.ts:546](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L546)

___

### vun\_TZ

• **vun\_TZ**

#### Defined in

[types/locale.ts:547](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L547)

___

### xog

• **xog**

#### Defined in

[types/locale.ts:548](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L548)

___

### xog\_UG

• **xog\_UG**

#### Defined in

[types/locale.ts:549](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L549)

___

### yav

• **yav**

#### Defined in

[types/locale.ts:550](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L550)

___

### yav\_CM

• **yav\_CM**

#### Defined in

[types/locale.ts:551](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L551)

___

### yo

• **yo**

#### Defined in

[types/locale.ts:552](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L552)

___

### yo\_NG

• **yo\_NG**

#### Defined in

[types/locale.ts:553](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L553)

___

### zh

• **zh**

#### Defined in

[types/locale.ts:554](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L554)

___

### zh\_Hans

• **zh\_Hans**

#### Defined in

[types/locale.ts:555](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L555)

___

### zh\_Hans\_AE

• **zh\_Hans\_AE**

#### Defined in

[types/locale.ts:556](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L556)

___

### zh\_Hans\_AR

• **zh\_Hans\_AR**

#### Defined in

[types/locale.ts:557](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L557)

___

### zh\_Hans\_AT

• **zh\_Hans\_AT**

#### Defined in

[types/locale.ts:558](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L558)

___

### zh\_Hans\_AU

• **zh\_Hans\_AU**

#### Defined in

[types/locale.ts:559](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L559)

___

### zh\_Hans\_BE

• **zh\_Hans\_BE**

#### Defined in

[types/locale.ts:560](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L560)

___

### zh\_Hans\_BG

• **zh\_Hans\_BG**

#### Defined in

[types/locale.ts:561](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L561)

___

### zh\_Hans\_BH

• **zh\_Hans\_BH**

#### Defined in

[types/locale.ts:562](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L562)

___

### zh\_Hans\_BR

• **zh\_Hans\_BR**

#### Defined in

[types/locale.ts:563](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L563)

___

### zh\_Hans\_BW

• **zh\_Hans\_BW**

#### Defined in

[types/locale.ts:564](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L564)

___

### zh\_Hans\_CA

• **zh\_Hans\_CA**

#### Defined in

[types/locale.ts:565](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L565)

___

### zh\_Hans\_CH

• **zh\_Hans\_CH**

#### Defined in

[types/locale.ts:566](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L566)

___

### zh\_Hans\_CL

• **zh\_Hans\_CL**

#### Defined in

[types/locale.ts:567](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L567)

___

### zh\_Hans\_CN

• **zh\_Hans\_CN**

#### Defined in

[types/locale.ts:568](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L568)

___

### zh\_Hans\_CO

• **zh\_Hans\_CO**

#### Defined in

[types/locale.ts:569](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L569)

___

### zh\_Hans\_CR

• **zh\_Hans\_CR**

#### Defined in

[types/locale.ts:570](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L570)

___

### zh\_Hans\_CY

• **zh\_Hans\_CY**

#### Defined in

[types/locale.ts:571](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L571)

___

### zh\_Hans\_CZ

• **zh\_Hans\_CZ**

#### Defined in

[types/locale.ts:572](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L572)

___

### zh\_Hans\_DE

• **zh\_Hans\_DE**

#### Defined in

[types/locale.ts:573](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L573)

___

### zh\_Hans\_DK

• **zh\_Hans\_DK**

#### Defined in

[types/locale.ts:574](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L574)

___

### zh\_Hans\_DO

• **zh\_Hans\_DO**

#### Defined in

[types/locale.ts:575](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L575)

___

### zh\_Hans\_EE

• **zh\_Hans\_EE**

#### Defined in

[types/locale.ts:576](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L576)

___

### zh\_Hans\_EG

• **zh\_Hans\_EG**

#### Defined in

[types/locale.ts:577](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L577)

___

### zh\_Hans\_ES

• **zh\_Hans\_ES**

#### Defined in

[types/locale.ts:578](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L578)

___

### zh\_Hans\_FI

• **zh\_Hans\_FI**

#### Defined in

[types/locale.ts:579](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L579)

___

### zh\_Hans\_GB

• **zh\_Hans\_GB**

#### Defined in

[types/locale.ts:580](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L580)

___

### zh\_Hans\_GE

• **zh\_Hans\_GE**

#### Defined in

[types/locale.ts:581](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L581)

___

### zh\_Hans\_GF

• **zh\_Hans\_GF**

#### Defined in

[types/locale.ts:582](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L582)

___

### zh\_Hans\_GH

• **zh\_Hans\_GH**

#### Defined in

[types/locale.ts:583](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L583)

___

### zh\_Hans\_GI

• **zh\_Hans\_GI**

#### Defined in

[types/locale.ts:584](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L584)

___

### zh\_Hans\_GR

• **zh\_Hans\_GR**

#### Defined in

[types/locale.ts:585](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L585)

___

### zh\_Hans\_HK

• **zh\_Hans\_HK**

#### Defined in

[types/locale.ts:586](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L586)

___

### zh\_Hans\_HR

• **zh\_Hans\_HR**

#### Defined in

[types/locale.ts:587](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L587)

___

### zh\_Hans\_HU

• **zh\_Hans\_HU**

#### Defined in

[types/locale.ts:588](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L588)

___

### zh\_Hans\_ID

• **zh\_Hans\_ID**

#### Defined in

[types/locale.ts:589](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L589)

___

### zh\_Hans\_IE

• **zh\_Hans\_IE**

#### Defined in

[types/locale.ts:590](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L590)

___

### zh\_Hans\_IL

• **zh\_Hans\_IL**

#### Defined in

[types/locale.ts:591](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L591)

___

### zh\_Hans\_IN

• **zh\_Hans\_IN**

#### Defined in

[types/locale.ts:592](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L592)

___

### zh\_Hans\_IS

• **zh\_Hans\_IS**

#### Defined in

[types/locale.ts:593](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L593)

___

### zh\_Hans\_IT

• **zh\_Hans\_IT**

#### Defined in

[types/locale.ts:594](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L594)

___

### zh\_Hans\_JO

• **zh\_Hans\_JO**

#### Defined in

[types/locale.ts:595](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L595)

___

### zh\_Hans\_JP

• **zh\_Hans\_JP**

#### Defined in

[types/locale.ts:596](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L596)

___

### zh\_Hans\_KR

• **zh\_Hans\_KR**

#### Defined in

[types/locale.ts:597](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L597)

___

### zh\_Hans\_KW

• **zh\_Hans\_KW**

#### Defined in

[types/locale.ts:598](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L598)

___

### zh\_Hans\_KY

• **zh\_Hans\_KY**

#### Defined in

[types/locale.ts:599](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L599)

___

### zh\_Hans\_LB

• **zh\_Hans\_LB**

#### Defined in

[types/locale.ts:600](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L600)

___

### zh\_Hans\_LI

• **zh\_Hans\_LI**

#### Defined in

[types/locale.ts:601](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L601)

___

### zh\_Hans\_LT

• **zh\_Hans\_LT**

#### Defined in

[types/locale.ts:602](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L602)

___

### zh\_Hans\_LU

• **zh\_Hans\_LU**

#### Defined in

[types/locale.ts:603](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L603)

___

### zh\_Hans\_LV

• **zh\_Hans\_LV**

#### Defined in

[types/locale.ts:604](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L604)

___

### zh\_Hans\_MA

• **zh\_Hans\_MA**

#### Defined in

[types/locale.ts:605](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L605)

___

### zh\_Hans\_MC

• **zh\_Hans\_MC**

#### Defined in

[types/locale.ts:606](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L606)

___

### zh\_Hans\_MG

• **zh\_Hans\_MG**

#### Defined in

[types/locale.ts:607](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L607)

___

### zh\_Hans\_MK

• **zh\_Hans\_MK**

#### Defined in

[types/locale.ts:608](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L608)

___

### zh\_Hans\_MO

• **zh\_Hans\_MO**

#### Defined in

[types/locale.ts:609](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L609)

___

### zh\_Hans\_MT

• **zh\_Hans\_MT**

#### Defined in

[types/locale.ts:610](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L610)

___

### zh\_Hans\_MU

• **zh\_Hans\_MU**

#### Defined in

[types/locale.ts:611](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L611)

___

### zh\_Hans\_MX

• **zh\_Hans\_MX**

#### Defined in

[types/locale.ts:612](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L612)

___

### zh\_Hans\_MY

• **zh\_Hans\_MY**

#### Defined in

[types/locale.ts:613](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L613)

___

### zh\_Hans\_NA

• **zh\_Hans\_NA**

#### Defined in

[types/locale.ts:614](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L614)

___

### zh\_Hans\_NL

• **zh\_Hans\_NL**

#### Defined in

[types/locale.ts:615](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L615)

___

### zh\_Hans\_NO

• **zh\_Hans\_NO**

#### Defined in

[types/locale.ts:616](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L616)

___

### zh\_Hans\_NZ

• **zh\_Hans\_NZ**

#### Defined in

[types/locale.ts:617](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L617)

___

### zh\_Hans\_OM

• **zh\_Hans\_OM**

#### Defined in

[types/locale.ts:618](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L618)

___

### zh\_Hans\_PE

• **zh\_Hans\_PE**

#### Defined in

[types/locale.ts:619](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L619)

___

### zh\_Hans\_PH

• **zh\_Hans\_PH**

#### Defined in

[types/locale.ts:620](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L620)

___

### zh\_Hans\_PK

• **zh\_Hans\_PK**

#### Defined in

[types/locale.ts:621](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L621)

___

### zh\_Hans\_PL

• **zh\_Hans\_PL**

#### Defined in

[types/locale.ts:622](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L622)

___

### zh\_Hans\_PR

• **zh\_Hans\_PR**

#### Defined in

[types/locale.ts:623](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L623)

___

### zh\_Hans\_PT

• **zh\_Hans\_PT**

#### Defined in

[types/locale.ts:624](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L624)

___

### zh\_Hans\_PY

• **zh\_Hans\_PY**

#### Defined in

[types/locale.ts:625](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L625)

___

### zh\_Hans\_QA

• **zh\_Hans\_QA**

#### Defined in

[types/locale.ts:626](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L626)

___

### zh\_Hans\_RO

• **zh\_Hans\_RO**

#### Defined in

[types/locale.ts:627](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L627)

___

### zh\_Hans\_RU

• **zh\_Hans\_RU**

#### Defined in

[types/locale.ts:628](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L628)

___

### zh\_Hans\_SA

• **zh\_Hans\_SA**

#### Defined in

[types/locale.ts:629](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L629)

___

### zh\_Hans\_SE

• **zh\_Hans\_SE**

#### Defined in

[types/locale.ts:630](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L630)

___

### zh\_Hans\_SG

• **zh\_Hans\_SG**

#### Defined in

[types/locale.ts:631](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L631)

___

### zh\_Hans\_SI

• **zh\_Hans\_SI**

#### Defined in

[types/locale.ts:633](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L633)

___

### zh\_Hans\_SK

• **zh\_Hans\_SK**

#### Defined in

[types/locale.ts:632](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L632)

___

### zh\_Hans\_TH

• **zh\_Hans\_TH**

#### Defined in

[types/locale.ts:634](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L634)

___

### zh\_Hans\_TR

• **zh\_Hans\_TR**

#### Defined in

[types/locale.ts:635](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L635)

___

### zh\_Hans\_TW

• **zh\_Hans\_TW**

#### Defined in

[types/locale.ts:636](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L636)

___

### zh\_Hans\_UA

• **zh\_Hans\_UA**

#### Defined in

[types/locale.ts:637](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L637)

___

### zh\_Hans\_US

• **zh\_Hans\_US**

#### Defined in

[types/locale.ts:638](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L638)

___

### zh\_Hans\_UY

• **zh\_Hans\_UY**

#### Defined in

[types/locale.ts:639](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L639)

___

### zh\_Hans\_VE

• **zh\_Hans\_VE**

#### Defined in

[types/locale.ts:640](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L640)

___

### zh\_Hans\_VN

• **zh\_Hans\_VN**

#### Defined in

[types/locale.ts:641](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L641)

___

### zh\_Hans\_ZA

• **zh\_Hans\_ZA**

#### Defined in

[types/locale.ts:642](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L642)

___

### zh\_Hant

• **zh\_Hant**

#### Defined in

[types/locale.ts:643](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L643)

___

### zh\_Hant\_AE

• **zh\_Hant\_AE**

#### Defined in

[types/locale.ts:644](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L644)

___

### zh\_Hant\_AR

• **zh\_Hant\_AR**

#### Defined in

[types/locale.ts:645](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L645)

___

### zh\_Hant\_AT

• **zh\_Hant\_AT**

#### Defined in

[types/locale.ts:646](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L646)

___

### zh\_Hant\_AU

• **zh\_Hant\_AU**

#### Defined in

[types/locale.ts:647](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L647)

___

### zh\_Hant\_BE

• **zh\_Hant\_BE**

#### Defined in

[types/locale.ts:648](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L648)

___

### zh\_Hant\_BG

• **zh\_Hant\_BG**

#### Defined in

[types/locale.ts:649](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L649)

___

### zh\_Hant\_BH

• **zh\_Hant\_BH**

#### Defined in

[types/locale.ts:650](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L650)

___

### zh\_Hant\_BR

• **zh\_Hant\_BR**

#### Defined in

[types/locale.ts:651](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L651)

___

### zh\_Hant\_BW

• **zh\_Hant\_BW**

#### Defined in

[types/locale.ts:652](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L652)

___

### zh\_Hant\_CA

• **zh\_Hant\_CA**

#### Defined in

[types/locale.ts:653](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L653)

___

### zh\_Hant\_CH

• **zh\_Hant\_CH**

#### Defined in

[types/locale.ts:654](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L654)

___

### zh\_Hant\_CL

• **zh\_Hant\_CL**

#### Defined in

[types/locale.ts:655](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L655)

___

### zh\_Hant\_CN

• **zh\_Hant\_CN**

#### Defined in

[types/locale.ts:656](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L656)

___

### zh\_Hant\_CO

• **zh\_Hant\_CO**

#### Defined in

[types/locale.ts:657](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L657)

___

### zh\_Hant\_CR

• **zh\_Hant\_CR**

#### Defined in

[types/locale.ts:658](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L658)

___

### zh\_Hant\_CY

• **zh\_Hant\_CY**

#### Defined in

[types/locale.ts:659](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L659)

___

### zh\_Hant\_CZ

• **zh\_Hant\_CZ**

#### Defined in

[types/locale.ts:660](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L660)

___

### zh\_Hant\_DE

• **zh\_Hant\_DE**

#### Defined in

[types/locale.ts:661](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L661)

___

### zh\_Hant\_DK

• **zh\_Hant\_DK**

#### Defined in

[types/locale.ts:662](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L662)

___

### zh\_Hant\_DO

• **zh\_Hant\_DO**

#### Defined in

[types/locale.ts:663](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L663)

___

### zh\_Hant\_EE

• **zh\_Hant\_EE**

#### Defined in

[types/locale.ts:664](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L664)

___

### zh\_Hant\_EG

• **zh\_Hant\_EG**

#### Defined in

[types/locale.ts:665](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L665)

___

### zh\_Hant\_ES

• **zh\_Hant\_ES**

#### Defined in

[types/locale.ts:666](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L666)

___

### zh\_Hant\_FI

• **zh\_Hant\_FI**

#### Defined in

[types/locale.ts:667](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L667)

___

### zh\_Hant\_GB

• **zh\_Hant\_GB**

#### Defined in

[types/locale.ts:668](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L668)

___

### zh\_Hant\_GE

• **zh\_Hant\_GE**

#### Defined in

[types/locale.ts:669](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L669)

___

### zh\_Hant\_GF

• **zh\_Hant\_GF**

#### Defined in

[types/locale.ts:670](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L670)

___

### zh\_Hant\_GH

• **zh\_Hant\_GH**

#### Defined in

[types/locale.ts:671](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L671)

___

### zh\_Hant\_GI

• **zh\_Hant\_GI**

#### Defined in

[types/locale.ts:672](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L672)

___

### zh\_Hant\_GR

• **zh\_Hant\_GR**

#### Defined in

[types/locale.ts:673](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L673)

___

### zh\_Hant\_HK

• **zh\_Hant\_HK**

#### Defined in

[types/locale.ts:674](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L674)

___

### zh\_Hant\_HR

• **zh\_Hant\_HR**

#### Defined in

[types/locale.ts:675](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L675)

___

### zh\_Hant\_HU

• **zh\_Hant\_HU**

#### Defined in

[types/locale.ts:676](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L676)

___

### zh\_Hant\_ID

• **zh\_Hant\_ID**

#### Defined in

[types/locale.ts:677](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L677)

___

### zh\_Hant\_IE

• **zh\_Hant\_IE**

#### Defined in

[types/locale.ts:678](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L678)

___

### zh\_Hant\_IL

• **zh\_Hant\_IL**

#### Defined in

[types/locale.ts:679](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L679)

___

### zh\_Hant\_IN

• **zh\_Hant\_IN**

#### Defined in

[types/locale.ts:680](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L680)

___

### zh\_Hant\_IS

• **zh\_Hant\_IS**

#### Defined in

[types/locale.ts:681](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L681)

___

### zh\_Hant\_IT

• **zh\_Hant\_IT**

#### Defined in

[types/locale.ts:682](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L682)

___

### zh\_Hant\_JO

• **zh\_Hant\_JO**

#### Defined in

[types/locale.ts:683](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L683)

___

### zh\_Hant\_JP

• **zh\_Hant\_JP**

#### Defined in

[types/locale.ts:684](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L684)

___

### zh\_Hant\_KR

• **zh\_Hant\_KR**

#### Defined in

[types/locale.ts:685](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L685)

___

### zh\_Hant\_KW

• **zh\_Hant\_KW**

#### Defined in

[types/locale.ts:686](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L686)

___

### zh\_Hant\_KY

• **zh\_Hant\_KY**

#### Defined in

[types/locale.ts:687](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L687)

___

### zh\_Hant\_LB

• **zh\_Hant\_LB**

#### Defined in

[types/locale.ts:688](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L688)

___

### zh\_Hant\_LI

• **zh\_Hant\_LI**

#### Defined in

[types/locale.ts:689](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L689)

___

### zh\_Hant\_LT

• **zh\_Hant\_LT**

#### Defined in

[types/locale.ts:690](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L690)

___

### zh\_Hant\_LU

• **zh\_Hant\_LU**

#### Defined in

[types/locale.ts:691](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L691)

___

### zh\_Hant\_LV

• **zh\_Hant\_LV**

#### Defined in

[types/locale.ts:692](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L692)

___

### zh\_Hant\_MA

• **zh\_Hant\_MA**

#### Defined in

[types/locale.ts:693](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L693)

___

### zh\_Hant\_MC

• **zh\_Hant\_MC**

#### Defined in

[types/locale.ts:694](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L694)

___

### zh\_Hant\_MG

• **zh\_Hant\_MG**

#### Defined in

[types/locale.ts:695](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L695)

___

### zh\_Hant\_MK

• **zh\_Hant\_MK**

#### Defined in

[types/locale.ts:696](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L696)

___

### zh\_Hant\_MO

• **zh\_Hant\_MO**

#### Defined in

[types/locale.ts:697](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L697)

___

### zh\_Hant\_MT

• **zh\_Hant\_MT**

#### Defined in

[types/locale.ts:698](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L698)

___

### zh\_Hant\_MU

• **zh\_Hant\_MU**

#### Defined in

[types/locale.ts:699](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L699)

___

### zh\_Hant\_MX

• **zh\_Hant\_MX**

#### Defined in

[types/locale.ts:700](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L700)

___

### zh\_Hant\_MY

• **zh\_Hant\_MY**

#### Defined in

[types/locale.ts:701](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L701)

___

### zh\_Hant\_NA

• **zh\_Hant\_NA**

#### Defined in

[types/locale.ts:702](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L702)

___

### zh\_Hant\_NL

• **zh\_Hant\_NL**

#### Defined in

[types/locale.ts:703](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L703)

___

### zh\_Hant\_NO

• **zh\_Hant\_NO**

#### Defined in

[types/locale.ts:704](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L704)

___

### zh\_Hant\_NZ

• **zh\_Hant\_NZ**

#### Defined in

[types/locale.ts:705](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L705)

___

### zh\_Hant\_OM

• **zh\_Hant\_OM**

#### Defined in

[types/locale.ts:706](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L706)

___

### zh\_Hant\_PE

• **zh\_Hant\_PE**

#### Defined in

[types/locale.ts:707](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L707)

___

### zh\_Hant\_PH

• **zh\_Hant\_PH**

#### Defined in

[types/locale.ts:708](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L708)

___

### zh\_Hant\_PK

• **zh\_Hant\_PK**

#### Defined in

[types/locale.ts:709](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L709)

___

### zh\_Hant\_PL

• **zh\_Hant\_PL**

#### Defined in

[types/locale.ts:710](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L710)

___

### zh\_Hant\_PR

• **zh\_Hant\_PR**

#### Defined in

[types/locale.ts:711](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L711)

___

### zh\_Hant\_PT

• **zh\_Hant\_PT**

#### Defined in

[types/locale.ts:712](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L712)

___

### zh\_Hant\_PY

• **zh\_Hant\_PY**

#### Defined in

[types/locale.ts:713](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L713)

___

### zh\_Hant\_QA

• **zh\_Hant\_QA**

#### Defined in

[types/locale.ts:714](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L714)

___

### zh\_Hant\_RO

• **zh\_Hant\_RO**

#### Defined in

[types/locale.ts:715](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L715)

___

### zh\_Hant\_RU

• **zh\_Hant\_RU**

#### Defined in

[types/locale.ts:716](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L716)

___

### zh\_Hant\_SA

• **zh\_Hant\_SA**

#### Defined in

[types/locale.ts:717](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L717)

___

### zh\_Hant\_SE

• **zh\_Hant\_SE**

#### Defined in

[types/locale.ts:718](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L718)

___

### zh\_Hant\_SG

• **zh\_Hant\_SG**

#### Defined in

[types/locale.ts:719](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L719)

___

### zh\_Hant\_SI

• **zh\_Hant\_SI**

#### Defined in

[types/locale.ts:721](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L721)

___

### zh\_Hant\_SK

• **zh\_Hant\_SK**

#### Defined in

[types/locale.ts:720](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L720)

___

### zh\_Hant\_TH

• **zh\_Hant\_TH**

#### Defined in

[types/locale.ts:722](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L722)

___

### zh\_Hant\_TR

• **zh\_Hant\_TR**

#### Defined in

[types/locale.ts:723](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L723)

___

### zh\_Hant\_TW

• **zh\_Hant\_TW**

#### Defined in

[types/locale.ts:724](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L724)

___

### zh\_Hant\_UA

• **zh\_Hant\_UA**

#### Defined in

[types/locale.ts:725](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L725)

___

### zh\_Hant\_US

• **zh\_Hant\_US**

#### Defined in

[types/locale.ts:726](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L726)

___

### zh\_Hant\_UY

• **zh\_Hant\_UY**

#### Defined in

[types/locale.ts:727](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L727)

___

### zh\_Hant\_VE

• **zh\_Hant\_VE**

#### Defined in

[types/locale.ts:728](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L728)

___

### zh\_Hant\_VN

• **zh\_Hant\_VN**

#### Defined in

[types/locale.ts:729](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L729)

___

### zh\_Hant\_ZA

• **zh\_Hant\_ZA**

#### Defined in

[types/locale.ts:730](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L730)

___

### zu

• **zu**

#### Defined in

[types/locale.ts:731](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L731)

___

### zu\_ZA

• **zu\_ZA**

#### Defined in

[types/locale.ts:732](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/locale.ts#L732)
