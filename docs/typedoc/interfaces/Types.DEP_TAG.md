[html-chunk-loader - v0.7.0](../README.md) / [Modules](../modules.md) / [Types](../modules/Types.md) / DEP\_TAG

# Interface: DEP\_TAG

[Types](../modules/Types.md).DEP_TAG

## Table of contents

### Properties

- [new](Types.DEP_TAG.md#new)
- [old](Types.DEP_TAG.md#old)
- [v\_change](Types.DEP_TAG.md#v_change)

## Properties

### new

• **new**: `string`

#### Defined in

[types/index.ts:119](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L119)

___

### old

• **old**: `string`

#### Defined in

[types/index.ts:118](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L118)

___

### v\_change

• **v\_change**: `string`

#### Defined in

[types/index.ts:120](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L120)
