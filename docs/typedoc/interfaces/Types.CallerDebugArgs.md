[html-chunk-loader - v0.7.0](../README.md) / [Modules](../modules.md) / [Types](../modules/Types.md) / CallerDebugArgs

# Interface: CallerDebugArgs

[Types](../modules/Types.md).CallerDebugArgs

## Table of contents

### Properties

- [debugger](Types.CallerDebugArgs.md#debugger)
- [errorSuppression](Types.CallerDebugArgs.md#errorsuppression)
- [logMode](Types.CallerDebugArgs.md#logmode)
- [logStrategy](Types.CallerDebugArgs.md#logstrategy)

## Properties

### debugger

• **debugger**: [`Debugger`](../modules/Types.md#debugger)

#### Defined in

[types/index.ts:50](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L50)

___

### errorSuppression

• **errorSuppression**: `boolean`

#### Defined in

[types/index.ts:47](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L47)

___

### logMode

• **logMode**: [`LogMode`](../modules/Types.md#logmode)

#### Defined in

[types/index.ts:48](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L48)

___

### logStrategy

• **logStrategy**: [`LogStrategy`](../modules/Types.md#logstrategy)

#### Defined in

[types/index.ts:49](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L49)
