[html-chunk-loader - v0.7.0](../README.md) / [Modules](../modules.md) / [Types](../modules/Types.md) / USSGOptions

# Interface: USSGOptions

[Types](../modules/Types.md).USSGOptions

## Hierarchy

- [`UGlobalOptions`](../modules/Types.md#uglobaloptions)

  ↳ **`USSGOptions`**

## Table of contents

### Properties

- [cleanup](Types.USSGOptions.md#cleanup)
- [debug](Types.USSGOptions.md#debug)
- [discoverPaths](Types.USSGOptions.md#discoverpaths)
- [errorSuppression](Types.USSGOptions.md#errorsuppression)
- [experimentalExtensions](Types.USSGOptions.md#experimentalextensions)
- [intlCode](Types.USSGOptions.md#intlcode)
- [loaderFile](Types.USSGOptions.md#loaderfile)
- [outPath](Types.USSGOptions.md#outpath)
- [partialInput](Types.USSGOptions.md#partialinput)
- [partials](Types.USSGOptions.md#partials)
- [pathRoot](Types.USSGOptions.md#pathroot)
- [templateInput](Types.USSGOptions.md#templateinput)
- [templates](Types.USSGOptions.md#templates)

## Properties

### cleanup

• `Optional` **cleanup**: `boolean`

#### Defined in

[types/index.ts:59](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L59)

___

### debug

• `Optional` **debug**: [`UUDebugConfig`](../modules/Types.md#uudebugconfig)

#### Inherited from

UGlobalOptions.debug

#### Defined in

[types/index.ts:27](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L27)

___

### discoverPaths

• `Optional` **discoverPaths**: `boolean`

#### Inherited from

UGlobalOptions.discoverPaths

#### Defined in

[types/index.ts:22](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L22)

___

### errorSuppression

• `Optional` **errorSuppression**: `boolean`

#### Inherited from

UGlobalOptions.errorSuppression

#### Defined in

[types/index.ts:25](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L25)

___

### experimentalExtensions

• `Optional` **experimentalExtensions**: `boolean`

#### Inherited from

UGlobalOptions.experimentalExtensions

#### Defined in

[types/index.ts:28](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L28)

___

### intlCode

• `Optional` **intlCode**: `string`

#### Inherited from

UGlobalOptions.intlCode

#### Defined in

[types/index.ts:26](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L26)

___

### loaderFile

• `Optional` **loaderFile**: `string`

#### Defined in

[types/index.ts:58](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L58)

___

### outPath

• `Optional` **outPath**: `string`

#### Defined in

[types/index.ts:57](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L57)

___

### partialInput

• `Optional` **partialInput**: `object`

#### Inherited from

UGlobalOptions.partialInput

#### Defined in

[types/index.ts:23](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L23)

___

### partials

• `Optional` **partials**: `string`

#### Inherited from

UGlobalOptions.partials

#### Defined in

[types/index.ts:21](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L21)

___

### pathRoot

• `Optional` **pathRoot**: `string`

#### Inherited from

UGlobalOptions.pathRoot

#### Defined in

[types/index.ts:19](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L19)

___

### templateInput

• `Optional` **templateInput**: `object`

#### Inherited from

UGlobalOptions.templateInput

#### Defined in

[types/index.ts:24](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L24)

___

### templates

• `Optional` **templates**: `string`

#### Inherited from

UGlobalOptions.templates

#### Defined in

[types/index.ts:20](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L20)
