[html-chunk-loader - v0.7.0](../README.md) / [Modules](../modules.md) / [Types](../modules/Types.md) / UDebugConfig

# Interface: UDebugConfig

[Types](../modules/Types.md).UDebugConfig

## Table of contents

### Properties

- [logFile](Types.UDebugConfig.md#logfile)
- [logMode](Types.UDebugConfig.md#logmode)
- [logStrategy](Types.UDebugConfig.md#logstrategy)

## Properties

### logFile

• `Optional` **logFile**: `string`

#### Defined in

[types/index.ts:32](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L32)

___

### logMode

• `Optional` **logMode**: [`LogMode`](../modules/Types.md#logmode)

#### Defined in

[types/index.ts:33](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L33)

___

### logStrategy

• `Optional` **logStrategy**: [`LogStrategy`](../modules/Types.md#logstrategy)

#### Defined in

[types/index.ts:34](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L34)
