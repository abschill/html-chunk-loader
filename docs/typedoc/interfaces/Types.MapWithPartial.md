[html-chunk-loader - v0.7.0](../README.md) / [Modules](../modules.md) / [Types](../modules/Types.md) / MapWithPartial

# Interface: MapWithPartial

[Types](../modules/Types.md).MapWithPartial

## Table of contents

### Properties

- [partialInput](Types.MapWithPartial.md#partialinput)

## Properties

### partialInput

• **partialInput**: `object`

#### Defined in

[types/index.ts:155](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L155)
