[html-chunk-loader - v0.7.0](../README.md) / [Modules](../modules.md) / [Types](../modules/Types.md) / HTMLChunkLoader

# Interface: HTMLChunkLoader

[Types](../modules/Types.md).HTMLChunkLoader

## Table of contents

### Properties

- [ctx](Types.HTMLChunkLoader.md#ctx)
- [template](Types.HTMLChunkLoader.md#template)

## Properties

### ctx

• **ctx**: [`LoaderContext`](../modules/Types.md#loadercontext)

#### Defined in

[types/index.ts:6](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L6)

___

### template

• **template**: [`HTMLChunkRenderFN`](../modules/Types.md#htmlchunkrenderfn)

#### Defined in

[types/index.ts:7](https://github.com/abschill/html-chunk-loader/blob/0db52a1/src/types/index.ts#L7)
