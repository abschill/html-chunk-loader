# Reference - CLI

## Static Site Generation

`npx html-chunk-loader ssg`

Runs the ssg module, it will load your settings using the ```ssg_config``` property in your project's ```package.json.hcl_config```, or in ```hcl-config.js```.
